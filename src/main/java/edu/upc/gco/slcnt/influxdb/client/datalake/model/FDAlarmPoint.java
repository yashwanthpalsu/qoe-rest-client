package edu.upc.gco.slcnt.influxdb.client.datalake.model;

import java.time.Instant;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Measurement(name = "Alarms")
public class FDAlarmPoint {

	@Column(name = "time")
	private Instant time;
	
	@Column(name = "ALARM_UUID", tag = true)
	//@Column(name = "alarm_uuid", tag = true)
	private String alarmUuid;
	
	@Column(name = "METRIC_NAME", tag = true)
	//@Column(name = "metric_name", tag = true)
	private String metricName;
	
	@Column(name = "NS_ID", tag = true)
	//@Column(name = "ns_id", tag = true)
	private String nsId;
	
	@Column(name = "OPERATION", tag = true)
	//@Column(name = "operation", tag = true)
	private String operation;
	
	@Column(name = "RESOURCE_ID", tag = true)
	//@Column(name = "resource_id", tag = true)
	private String resourceId;
	
	@Column(name = "SEVERITY", tag = true)
	//@Column(name = "severity", tag = true)
	private String severity;
	
	@Column(name = "STATUS", tag = true)
	//@Column(name = "status", tag = true)
	private String status;
	
	@Column(name = "THRESHOLD_VALUE", tag = true)
	//@Column(name = "threshold_value", tag = true)
	private String thresholdValue;
	
	@Column(name = "VDU_NAME", tag = true)
	//@Column(name = "vdu_name", tag = true)
	private String vduName;
	
	@Column(name = "VNF_MEMBER_INDEX", tag = true)
	//@Column(name = "vnf_member_index", tag = true)
	private String vnfMemberIndex;
	
	@Column(name = "DESCRIPTION")
	//@Column(name = "description")
	private String description;
	
	@Column(name = "DSP_ID")
	private String dspId;
	
	@Column(name = "GEO_AGG_CODE")
	//@Column(name = "geo_agg_code")
	private String geoAggCode;
	
	@Column(name = "ORIGINATING_SUBSYSTEM")
	//@Column(name = "originating_subsystem")
	private String originatingSubsystem;
	
	@Column(name = "START_DATE")
	//@Column(name = "start_date")
	private String startDate;

	public Instant getTime() {
		return time;
	}

	public String getAlarmUuid() {
		return alarmUuid;
	}

	public String getMetricName() {
		return metricName;
	}

	public String getNsId() {
		return nsId;
	}

	public String getOperation() {
		return operation;
	}

	public String getResourceId() {
		return resourceId;
	}

	public String getSeverity() {
		return severity;
	}

	public String getStatus() {
		return status;
	}

	public String getThresholdValue() {
		return thresholdValue;
	}

	public String getVduName() {
		return vduName;
	}

	public String getVnfMemberIndex() {
		return vnfMemberIndex;
	}

	public String getDescription() {
		return description;
	}

	public String getDspId() {
		return dspId;
	}

	public String getGeoAggCode() {
		return geoAggCode;
	}

	public String getOriginatingSubsystem() {
		return originatingSubsystem;
	}

	public String getStartDate() {
		return startDate;
	}
	
	private boolean evaluateAsInt (String value1, String value2, String condition)
	{
		int intValue1 = Integer.parseInt(value1);
		int intValue2 = Integer.parseInt(value2);
		
		switch (condition)
		{
			case "<":
				return (intValue1 < intValue2);
			case "<=":
				return (intValue1 <= intValue2);
			case ">":
				return (intValue1 > intValue2);
			case ">=":
				return (intValue1 >= intValue2);
		}
		
		return false;
	}
	
	private boolean evaluateAsString (String value1, String value2, String condition)
	{
		switch (condition)
		{
			case "=":
				return value1.equals(value2);
			case "!=":
				return !value1.equals(value2);
		}
		
		return false;
	}
	
	public boolean evaluate (String name, String value, String condition)
	{
		switch (name)
		{
			case "metric_name":
				return evaluateAsString (metricName, value, condition);
			case "severity":
				return evaluateAsString (severity, value, condition);
			case "threshold_value":
				return evaluateAsInt (thresholdValue, value, condition);
			case "METRIC_NAME":
				return evaluateAsString (metricName, value, condition);
			case "SEVERITY":
				return evaluateAsString (severity, value, condition);
			case "THRESHOLD_VALUE":
				return evaluateAsInt (thresholdValue, value, condition);
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		return "FDAlarmPoint [time=" + time + ", alarmUuid=" + alarmUuid + ", metricName=" + metricName + ", nsId="
				+ nsId + ", operation=" + operation + ", resourceId=" + resourceId + ", severity=" + severity
				+ ", status=" + status + ", thresholdValue=" + thresholdValue + ", vduName=" + vduName
				+ ", vnfMemberIndex=" + vnfMemberIndex + ", description=" + description + ", geoAggCode=" + geoAggCode
				+ ", originatingSubsystem=" + originatingSubsystem + ", startDate=" + startDate + "]";
	}
}
