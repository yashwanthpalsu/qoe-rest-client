package edu.upc.gco.slcnt.influxdb.client.datalake.model;

import java.time.Instant;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name = "action")
public class NNActionPoint {

	@Column(name = "time")
	private Instant time;
	
	@Column(name = "event_ts")
	private Instant eventTS;
	
	@Column(name = "tenantid")
	private String tenantId;

	public Instant getTime() {
		return time;
	}

	public void setTime(Instant time) {
		this.time = time;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
	public Instant getEventTS() {
		return eventTS;
	}

	public void setEventTS(Instant eventTS) {
		this.eventTS = eventTS;
	}

	@Override
	public String toString()
	{
		String str = "Time = " + this.time + " Event TS = " + this.eventTS + " TenantId = " + this.tenantId;
		
		return str;
	}
}
