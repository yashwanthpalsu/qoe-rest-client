package edu.upc.gco.slcnt.rest.client.serviceo.interfaces.mngmt.elements;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SliceInfo {

	@JsonProperty("id")
	private String sliceInstanceId;
	
	@JsonProperty("type")
	private String sliceType;
	
	@JsonProperty("nsp")
	private String nspName;
	
	public SliceInfo()
	{
		
	}
	
	public SliceInfo (String id, String type, String nsp)
	{
		this.sliceInstanceId = id;
		this.sliceType = type;
		this.nspName = nsp;
	}

	public String getSliceInstanceId() {
		return sliceInstanceId;
	}

	public void setSliceInstanceId(String sliceInstanceId) {
		this.sliceInstanceId = sliceInstanceId;
	}

	public String getSliceType() {
		return sliceType;
	}

	public void setSliceType(String sliceType) {
		this.sliceType = sliceType;
	}

	public String getNspName() {
		return nspName;
	}

	public void setNspName(String nspName) {
		this.nspName = nspName;
	}
}
