package edu.upc.gco.slcnt.rest.client.controlplane.interfaces.server.messages;

import java.util.HashMap;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class InterDomainConnectionRequest implements InterfaceMessage {

	private String level;						//L2,L3
	private String operation;					//ADD, DELETE
	private String segment;						//DC,WAN,RAN
	private HashMap<String,String> parameters;	//PARAM LIST
	
	public InterDomainConnectionRequest ()
	{
		
	}
	
	public InterDomainConnectionRequest (String level, String operation, String segment, HashMap<String,String> parameters)
	{
		this.level = level;
		this.operation = operation;
		this.segment = segment;
		this.parameters = parameters;
	}
	
	public String getLevel() {
		return level;
	}

	public String getOperation() {
		return operation;
	}

	public String getSegment() {
		return segment;
	}

	public HashMap<String,String> getParameters() {
		return parameters;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (level == null) throw new MalformattedElementException("Create Inter-Domain Connection without specifying level");
		if (operation == null) throw new MalformattedElementException("Create Inter-Domain Connection without specifying operation");
		if (segment == null) throw new MalformattedElementException("Create Inter-Domain Connection without specifying segment");
	}

}
