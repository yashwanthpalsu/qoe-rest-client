package edu.upc.gco.slcnt.rest.client.openstack.model.keystone;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Role {

	@JsonProperty("id")
	private String id;
	
	@JsonProperty("name")
	private String name;
}
