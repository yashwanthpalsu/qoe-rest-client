package edu.upc.gco.slcnt.rest.client.openstack;

import edu.upc.gco.slcnt.rest.client.openstack.keystone.KeystoneApi;
import edu.upc.gco.slcnt.rest.client.openstack.nova.NovaApi;

public class OpenStackApi {

	// Constructor
	private String ksIP;
	private String ksPort;
	private String novaIP;
	private String novaPort;
	private String adminTenant = "admin";
	private String tenantName;	//Project name
	private String userName;
	private String userPwd;
	private String adminUser;
	private String adminPwd;
	
	// Work
	private String ksUrl;
	private KeystoneApi ksClient;
	private String novaUrl;
	private NovaApi novaClient;
	private String adminToken;
	private String projectToken;
	private String userToken;
	private String userId;
	private String projectId;
	
	public OpenStackApi (String ksIP, String ksPort, String novaIP, String novaPort, String tenantName, String userName, String userPwd, String adminUser, String adminPwd)
	{
		this.ksIP = ksIP;
		this.ksPort = ksPort;
		this.novaIP = novaIP;
		this.novaPort = novaPort;
		this.tenantName = tenantName;
		this.userName = userName;
		this.userPwd = userPwd;
		this.adminUser = adminUser;
		this.adminPwd = adminPwd;
		
		this.ksUrl = "http://" + this.ksIP + ":" + this.ksPort + "/v3";
		this.ksClient = new KeystoneApi();
		/*
		this.adminToken = this.ksClient.getAdminToken(this.ksUrl + "/auth/tokens", this.adminUser, this.adminPwd);
		this.userId = this.ksClient.getUserId(this.ksUrl + "/users", this.userName, this.adminToken);
		this.projectId = this.ksClient.getProjectId(this.ksUrl + "/users/" + this.userId + "/projects", this.tenantName, this.adminToken);
		this.projectToken = this.ksClient.getProjectToken(this.ksUrl + "/auth/tokens", this.userName, this.userPwd, this.projectId);
		*/
		
		// Get Unscoped Token associated to the admin credentials
		this.adminToken = this.ksClient.getAdminToken(this.ksUrl + "/auth/tokens", this.adminUser, this.adminPwd);
		// Create Admin Project Scoped Token
		this.projectToken = this.ksClient.createProjectToken(this.ksUrl + "/auth/tokens", this.adminTenant, this.adminToken);
		// Get userId
		this.userId = this.ksClient.getUserId(this.ksUrl + "/users", this.userName, this.projectToken);
		// Get projectId
		this.projectId = this.ksClient.getProjectId(this.ksUrl + "/users/" + this.userId + "/projects", this.tenantName, this.projectToken);
		// Get Unscoped Token associated to the project admin credentials
		this.userToken = this.ksClient.getAdminToken(this.ksUrl + "/auth/tokens", this.userName, this.userPwd);
		// Create Tenant Project Scoped Token
		this.projectToken = this.ksClient.createProjectToken(this.ksUrl + "/auth/tokens", this.tenantName, this.userToken);
		
		this.novaUrl = "http://" + this.novaIP + ":" + this.novaPort;
		this.novaClient = new NovaApi();
	}
	
	public void rescale (String vmId, String flavorName)
	{
		String flavorId = this.novaClient.getFlavorId(this.novaUrl + "/v2/" + this.projectId + "/flavors", /*this.adminToken*/this.projectToken, flavorName);
		this.novaClient.resize(this.novaUrl  + this.projectId + "/servers/" + vmId, this.projectToken, flavorId);
	}
	
	public String liveMigrate (String vmId, String hostId)
	{
		return this.novaClient.liveMigrate(this.novaUrl + "/v2/" + this.projectId + "/servers/" + vmId, this.projectToken, hostId);
	}
}
