package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

public enum CPSStatus {

	REGISTERED	("REGISTERED"),
	SUSPENDED 	("SUSPENDED");
	
	protected String value;
	
	private CPSStatus(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
