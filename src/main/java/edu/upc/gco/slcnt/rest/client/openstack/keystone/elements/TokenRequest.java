package edu.upc.gco.slcnt.rest.client.openstack.keystone.elements;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenRequest {

	@JsonProperty("id")
	private String id = null;

	public TokenRequest (String id)
	{
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
