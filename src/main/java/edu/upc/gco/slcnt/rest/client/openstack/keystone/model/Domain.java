package edu.upc.gco.slcnt.rest.client.openstack.keystone.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Domain {

	@JsonProperty("id")
	private String id = null;
	
	public Domain (String id)
	{
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
