package edu.upc.gco.slcnt.rest.client.osm.model.catalogue.nsDescriptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.rest.client.osm.model.common.AdminInfo;
//import edu.upc.gco.slcnt.rest.client.osm.model.common.VNFDRefElement;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.nsDescriptor.NSDescriptor;

/**
 * NSD base information object.
 * 
 * @author fernando
 *
 */
public class NSDescriptorEx extends NSDescriptor {

	/*@JsonProperty("constituent-vnfd")
	private List<VNFDRefElement> vnfdList;
	@JsonProperty("version")
	private String version;
	@JsonProperty("vld")
	private List<VLDescriptor> vldList;
	@JsonProperty("description")
	private String description;
	@JsonProperty("name")
	private String name;
	@JsonProperty("id")
	private String id;
	@JsonProperty("short-name")
	private String shortName;*/
	@JsonProperty("_id")
	private String nsdId;
	@JsonProperty("_admin")
	private AdminInfo adminInfo;
	/*@JsonProperty("vendor")
	private String vendor;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();*/
	
	/*public List<VNFDRefElement> getVnfdList() {
		return vnfdList;
	}
	public void setVnfdList(List<VNFDRefElement> vnfdList) {
		this.vnfdList = vnfdList;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public List<VLDescriptor> getVldList() {
		return vldList;
	}
	public void setVldList(List<VLDescriptor> vldList) {
		this.vldList = vldList;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}*/
	public String getNsdId() {
		return nsdId;
	}
	public void setNsdId(String nsdId) {
		this.nsdId = nsdId;
	}
	public AdminInfo getAdminInfo() {
		return adminInfo;
	}
	public void setAdminInfo(AdminInfo adminInfo) {
		this.adminInfo = adminInfo;
	}
	/*public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
    
	@Override
	public String toString() {
		return "NSDescriptor [vnfdList=" + vnfdList + ", version=" + version + ", vldList=" + vldList + ", description="
				+ description + ", name=" + name + ", id=" + id + ", shortName=" + shortName + ", nsdId=" + nsdId
				+ ", adminInfo=" + adminInfo + ", vendor=" + vendor + ", otherProperties=" + otherProperties + "]";
	}*/
	@Override
	public String toString() {
		return "NSDescriptorEx [nsdId=" + nsdId + ", adminInfo=" + adminInfo + ", getId()=" + getId() + ", getLogo()="
				+ getLogo() + ", getName()=" + getName() + ", getShortName()=" + getShortName() + ", getVendor()="
				+ getVendor() + ", getVersion()=" + getVersion() + ", getDescription()=" + getDescription()
				+ ", getConstituentVNFDs()=" + getConstituentVNFDs() + ", getXpathInputParameters()="
				+ getXpathInputParameters() + ", getIpProfiles()=" + getIpProfiles() + ", getPlacementGroups()="
				+ getPlacementGroups() + ", getVldList()=" + getVldList() + ", getConnectionPoints()="
				+ getConnectionPoints() + ", getScalingGroupDescriptors()=" + getScalingGroupDescriptors()
				+ ", getVnffgd()=" + getVnffgd() + ", getInitialServicePrimitives()=" + getInitialServicePrimitives()
				+ ", getTerminateServicePrimitives()=" + getTerminateServicePrimitives() + ", getParameterPools()="
				+ getParameterPools() + ", getKeyPairs()=" + getKeyPairs() + ", getUser()=" + getUser()
				+ ", getVnfDependencies()=" + getVnfDependencies() + ", getMonitoringParams()=" + getMonitoringParams()
				+ ", getServicePrimitives()=" + getServicePrimitives() + ", any()=" + any() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
}
