package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This object contains the "_admin" information provided by the NS Instance.
 * The project fields (projects_write and projects_read) contain the project Id of the OSM project.
 * 
 * @author fernando
 *
 */
public class AdminObject {

	@JsonProperty("deployed")
	private DeploymentInfo deployed;
	@JsonProperty("projects_write")
	private List<String> projectsWrite;		//OSM project IDs
	@JsonProperty("nslcmop")
	private String nsLcmOp;					//TODO: Check type
	@JsonProperty("modified")
	private float modified;
	@JsonProperty("projects_read")
	private List<String> projectsRead;		// OSM project IDs
	@JsonProperty("nsState")
	private String nsState;
	@JsonProperty("created")
	private float created;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public DeploymentInfo getDeployed() {
		return deployed;
	}
	public void setDeployed(DeploymentInfo deployed) {
		this.deployed = deployed;
	}
	public List<String> getProjectsWrite() {
		return projectsWrite;
	}
	public void setProjectsWrite(List<String> projectsWrite) {
		this.projectsWrite = projectsWrite;
	}
	public String getNsLcmOp() {
		return nsLcmOp;
	}
	public void setNsLcmOp(String nsLcmOp) {
		this.nsLcmOp = nsLcmOp;
	}
	public float getModified() {
		return modified;
	}
	public void setModified(float modified) {
		this.modified = modified;
	}
	public List<String> getProjectsRead() {
		return projectsRead;
	}
	public void setProjectsRead(List<String> projectsRead) {
		this.projectsRead = projectsRead;
	}
	public String getNsState() {
		return nsState;
	}
	public void setNsState(String nsState) {
		this.nsState = nsState;
	}
	public float getCreated() {
		return created;
	}
	public void setCreated(float created) {
		this.created = created;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
	
    @Override
	public String toString() {
		return "AdminObject [deployed=" + deployed + ", projectsWrite=" + projectsWrite + ", nsLcmOp=" + nsLcmOp
				+ ", modified=" + modified + ", projectsRead=" + projectsRead + ", nsState=" + nsState + ", created="
				+ created + ", otherProperties=" + otherProperties + "]";
	}
}
