package edu.upc.gco.slcnt.rest.client.common.interfaces.elements;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceInformationElement;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class EndPointPair implements InterfaceInformationElement {

	private EndPoint endPointA;
	private EndPoint endPointB;
	
	public EndPointPair ()
	{
		
	}
	
	public EndPointPair (EndPoint endPointA, EndPoint endPointB)
	{
		this.endPointA = endPointA;
		this.endPointB = endPointB;
	}
	
	public EndPoint getEndPointA() {
		return endPointA;
	}

	public EndPoint getEndPointB() {
		return endPointB;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		// TODO Auto-generated method stub
		
	}

}
