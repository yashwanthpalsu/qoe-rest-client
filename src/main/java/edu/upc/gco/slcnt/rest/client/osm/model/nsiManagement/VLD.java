package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Virtual Link Descriptor associated to the NS Instance.
 * 
 * @author fernando
 *
 */
public class VLD {

	@JsonProperty("status-detailed")
	private String statusDetailed;
	@JsonProperty("vim-id")
	private String vimId;					// VIM Id at OpenStack??
	@JsonProperty("status")
	private String status;					// Example value: "ACTIVE"
	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("vim-network-name")
	private String vimNetworkName;			// == name
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public String getStatusDetailed() {
		return statusDetailed;
	}
	public void setStatusDetailed(String statusDetailed) {
		this.statusDetailed = statusDetailed;
	}
	public String getVimId() {
		return vimId;
	}
	public void setVimId(String vimId) {
		this.vimId = vimId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVimNetworkName() {
		return vimNetworkName;
	}
	public void setVimNetworkName(String vimNetworkName) {
		this.vimNetworkName = vimNetworkName;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
    
	@Override
	public String toString() {
		return "VLD [statusDetailed=" + statusDetailed + ", vimId=" + vimId + ", status=" + status + ", id=" + id
				+ ", name=" + name + ", vimNetworkName=" + vimNetworkName + ", otherProperties=" + otherProperties
				+ "]";
	}
}
