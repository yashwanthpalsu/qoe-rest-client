package edu.upc.gco.slcnt.rest.client.osm.model.catalogue.nsDescriptor;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VNFDCPRefElement {

	@JsonProperty("member-vnf-index-ref")
	private int vnfIndex;
	@JsonProperty("vnfd-connection-point-ref")
	private String vnfdCPName;
	@JsonProperty("vnfd-id-ref")
	private String vnfdName;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public int getVnfIndex() {
		return vnfIndex;
	}
	public void setVnfIndex(int vnfIndex) {
		this.vnfIndex = vnfIndex;
	}
	public String getVnfdCPName() {
		return vnfdCPName;
	}
	public void setVnfdCPName(String vnfdCPName) {
		this.vnfdCPName = vnfdCPName;
	}
	public String getVnfdName() {
		return vnfdName;
	}
	public void setVnfdName(String vnfdName) {
		this.vnfdName = vnfdName;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
    
	@Override
	public String toString() {
		return "VNFDCPRefElement [vnfIndex=" + vnfIndex + ", vnfdCPName=" + vnfdCPName + ", vnfdName=" + vnfdName
				+ ", otherProperties=" + otherProperties + "]";
	}
}
