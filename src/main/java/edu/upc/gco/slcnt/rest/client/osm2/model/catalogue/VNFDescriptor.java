package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VNFDescriptor {

	@JsonProperty("vdu")
	private List<VDU> vduList;
	@JsonProperty("version")
	private String version;
	@JsonProperty("mgmt-interface")
	private MgmtInterface mgmtIf;
	@JsonProperty("description")
	private String description;
	@JsonProperty("name")
	private String name;
	@JsonProperty("id")
	private String id;
	@JsonProperty("short-name")
	private String shortName;
	@JsonProperty("_id")
	private String vnfdId;
	@JsonProperty("_admin")
	private AdminInfo adminInfo;
	@JsonProperty("connection-point")
	private List<ConnectionPoint> cpList;
	@JsonProperty("vendor")
	private String vendor;
	
	public List<VDU> getVduList() {
		return vduList;
	}
	public void setVduList(List<VDU> vduList) {
		this.vduList = vduList;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public MgmtInterface getMgmtIf() {
		return mgmtIf;
	}
	public void setMgmtIf(MgmtInterface mgmtIf) {
		this.mgmtIf = mgmtIf;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getVnfdId() {
		return vnfdId;
	}
	public void setVnfdId(String vnfdId) {
		this.vnfdId = vnfdId;
	}
	public AdminInfo getAdminInfo() {
		return adminInfo;
	}
	public void setAdminInfo(AdminInfo adminInfo) {
		this.adminInfo = adminInfo;
	}
	public List<ConnectionPoint> getCpList() {
		return cpList;
	}
	public void setCpList(List<ConnectionPoint> cpList) {
		this.cpList = cpList;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
}
