package edu.upc.gco.slcnt.rest.client.osm.osmManagement;

import org.springframework.http.ResponseEntity;

public interface OSMManagementInterface {

	ResponseEntity<?> getAuthenticationToken();
	ResponseEntity<?> getProjects();
	ResponseEntity<?> getVimInfo(String vimAccountId);
	ResponseEntity<?> getVimInfoList();
}
