package edu.upc.gco.slcnt.rest.client.openstack.keystone;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.rest.client.Utils;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.model.Authentication;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.model.AuthenticationRequest;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.model.Domain;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.model.Identity;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.model.Password;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.model.Project;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.model.Scope;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.model.Token;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.model.User;

public class KeystoneApi {

	private Log logger = LogFactory.getLog(KeystoneApi.class);
	
	public String getUserId (String url, String userName, String token)
	{
		String userId = "";
		logger.info("URL: " + url + " -- Credentials: " + userName + "/" + token);
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Auth-Token", token);
		
		HttpEntity<String> getUsers = new HttpEntity<String>(headers);
		ResponseEntity<String> re = rt.exchange(url, HttpMethod.GET, getUsers, String.class);
		
		JsonNode json = null;
		try {
			json = new ObjectMapper().readTree(re.getBody());
			JsonNode users = json.get("users");
			if (users.isArray())
			{
				for (int u = 0; u < users.size(); u++)
				{
					String name = users.get(u).get("name").asText();
					if (name.equals(userName))
					{
						userId = users.get(u).get("id").asText();
						return userId;
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return userId;
	}
	
	public String getProjectId (String url, String projectName, String token)
	{
		String projectId = "";
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Auth-Token", token);
		
		HttpEntity<String> getProjects = new HttpEntity<String>(headers);
		ResponseEntity<String> re = rt.exchange(url, HttpMethod.GET, getProjects, String.class);
		
		JsonNode json = null;
		try {
			json = new ObjectMapper().readTree(re.getBody());
			JsonNode projects = json.get("projects");
			
			if (projects.isArray())
			{
				for (int p = 0; p < projects.size(); p++)
				{
					String name = projects.get(p).get("name").asText();
					if (name.equals(projectName))
					{
						projectId = projects.get(p).get("id").asText();
						return projectId;
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return projectId;
	}
	
	public String getAdminToken(String url, String adminUser, String adminPwd)
	{
		String token = null;
		logger.info("URL: " + url + " -- Credentials: " + adminUser + "/" + adminPwd);
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Domain domain = new Domain("default");
		User user = new User (adminUser, domain, adminPwd);
		Password password = new Password (user);
		ArrayList<String> methods = new ArrayList<String>();
		methods.add("password");
		Identity identity = new Identity(methods, password, null);
		Authentication auth = new Authentication(identity, null);
		AuthenticationRequest authRequest = new AuthenticationRequest(auth);
		
		try {
			String json = new ObjectMapper().writeValueAsString(authRequest);
			logger.info("JSON: " + json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpEntity<AuthenticationRequest> tokenAuth = new HttpEntity<AuthenticationRequest>(authRequest, headers);
		ResponseEntity<Void> re = rt.exchange(url, HttpMethod.POST, tokenAuth, Void.class);
		logger.info("STATUS: " + re.getStatusCodeValue());
		if (re.getStatusCode() == HttpStatus.CREATED)
		{
			token = re.getHeaders().get("X-Subject-Token").get(0);
		}
		
		return token;
	}
	
	public String getProjectToken(String url, String userId, String userPwd, String projectId)
	{
		String token = null;
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		User user = new User (userId, userPwd);
		Password password = new Password (user);
		ArrayList<String> methods = new ArrayList<String>();
		methods.add("password");
		Identity identity = new Identity(methods, password, null);
		Project project = new Project (projectId, null, null);
		Scope scope = new Scope (project);
		Authentication auth = new Authentication(identity, scope);
		AuthenticationRequest authRequest = new AuthenticationRequest(auth);
		
		HttpEntity<AuthenticationRequest> tokenAuth = new HttpEntity<AuthenticationRequest>(authRequest, headers);
		ResponseEntity<Void> re = rt.exchange(url, HttpMethod.POST, tokenAuth, Void.class);
		if (re.getStatusCode() == HttpStatus.CREATED)
			token = re.getHeaders().get("X-Subject-Token").get(0);
		
		return token;
	}
	
	public String createProjectToken(String url, String projectName, String unscopedToken)
	{
		String token = null;
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Token usToken = new Token(unscopedToken);
		ArrayList<String> methods = new ArrayList<String>();
		methods.add("token");
		Identity identity = new Identity(methods, null, usToken);
		Domain domain = new Domain ("default");
		Project project = new Project (null, domain, projectName);
		Scope scope = new Scope(project);
		Authentication auth = new Authentication(identity, scope);
		AuthenticationRequest authRequest = new AuthenticationRequest(auth);
		
		HttpEntity<AuthenticationRequest> tokenAuth = new HttpEntity<AuthenticationRequest>(authRequest, headers);
		ResponseEntity<Void> re = rt.exchange(url, HttpMethod.POST, tokenAuth, Void.class);
		if (re.getStatusCode() == HttpStatus.CREATED)
			token = re.getHeaders().get("X-Subject-Token").get(0);
		
		return token;
	}
}
