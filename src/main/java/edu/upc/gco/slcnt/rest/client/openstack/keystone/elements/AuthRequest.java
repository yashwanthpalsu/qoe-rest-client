package edu.upc.gco.slcnt.rest.client.openstack.keystone.elements;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthRequest {

	@JsonProperty("auth")
	private Authentication auth = null;
	
	public AuthRequest (Authentication auth)
	{
		this.auth = auth;
	}

	public Authentication getAuth() {
		return auth;
	}

	public void setAuth(Authentication auth) {
		this.auth = auth;
	}
}
