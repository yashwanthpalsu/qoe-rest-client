package edu.upc.gco.slcnt.rest.client.osm.model.osmManagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectAdminInfo {

	@JsonProperty("modified")
	private String modified;
	
	@JsonProperty("created")
	private String created;

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return "ProjectAdminInfo [modified=" + modified + ", created=" + created + "]";
	}
}
