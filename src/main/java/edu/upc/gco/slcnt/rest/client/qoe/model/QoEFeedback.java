package edu.upc.gco.slcnt.rest.client.qoe.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * QoEObject
 */

public class QoEFeedback   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("value")
  private String value = null;

  public QoEFeedback id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public QoEFeedback value(String value) {
    this.value = value;
    return this;
  }

  /**
   * Get value
   * @return value
  **/
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    QoEFeedback qoEObject = (QoEFeedback) o;
    return Objects.equals(this.id, qoEObject.id) &&
        Objects.equals(this.value, qoEObject.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class QoEObject {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

