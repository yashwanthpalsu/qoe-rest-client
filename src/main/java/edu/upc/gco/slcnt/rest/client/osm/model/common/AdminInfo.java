package edu.upc.gco.slcnt.rest.client.osm.model.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.nextworks.nfvmano.libs.osmr4PlusDataModel.nsDescriptor.KeyPair;

/**
 * This object corresponds to the "_admin" information associated to the NSD.
 * 
 * @author fernando
 *
 */
public class AdminInfo {

	@JsonProperty("type")
	private String type;
	@JsonProperty("projects_write")
	private List<String> projectsWrite;				// OSM Project Ids
	@JsonProperty("onboardingState")
	private String onboardingState;					// Example value: ONBOARDED
	@JsonProperty("operationalState")
	private String operationalState;				// Example value: ENABLED
	@JsonProperty("projects_read")
	private List<String> projectsRead;
	@JsonProperty("modified")
	private float modified;
	@JsonProperty("storage")
	private Storage storage;
	@JsonProperty("usageState")
	private String usageState;						// Example value: NOT_IN_USE
	@JsonProperty("userDefinedData")
	private KeyPair userDefinedData;
	@JsonProperty("created")
	private float created;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public List<String> getProjectsWrite() {
		return projectsWrite;
	}
	public void setProjectsWrite(List<String> projectsWrite) {
		this.projectsWrite = projectsWrite;
	}
	public String getOnboardingState() {
		return onboardingState;
	}
	public void setOnboardingState(String onboardingState) {
		this.onboardingState = onboardingState;
	}
	public String getOperationalState() {
		return operationalState;
	}
	public void setOperationalState(String operationalState) {
		this.operationalState = operationalState;
	}
	public List<String> getProjectsRead() {
		return projectsRead;
	}
	public void setProjectsRead(List<String> projectsRead) {
		this.projectsRead = projectsRead;
	}
	public float getModified() {
		return modified;
	}
	public void setModified(float modified) {
		this.modified = modified;
	}
	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public String getUsageState() {
		return usageState;
	}
	public void setUsageState(String usageState) {
		this.usageState = usageState;
	}
	public KeyPair getUserDefinedData() {
		return userDefinedData;
	}
	public void setUserDefinedData(KeyPair userDefinedData) {
		this.userDefinedData = userDefinedData;
	}
	public float getCreated() {
		return created;
	}
	public void setCreated(float created) {
		this.created = created;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
    
	@Override
	public String toString() {
		return "AdminInfo [type=" + type + ", projectsWrite=" + projectsWrite + ", onboardingState=" + onboardingState
				+ ", operationalState=" + operationalState + ", projectsRead=" + projectsRead + ", modified=" + modified
				+ ", storage=" + storage + ", usageState=" + usageState + ", userDefinedData=" + userDefinedData
				+ ", created=" + created + ", otherProperties=" + otherProperties + "]";
	}
}
