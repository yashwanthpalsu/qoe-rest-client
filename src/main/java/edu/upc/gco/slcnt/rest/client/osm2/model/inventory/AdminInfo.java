package edu.upc.gco.slcnt.rest.client.osm2.model.inventory;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdminInfo {

	@JsonProperty("deployed")
	private DeploymentInfo deployed;
	@JsonProperty("projects_write")
	private List<String> projectsWrite;
	@JsonProperty("nslcmop")
	private String nsLcmOp;	//TODO: Check type
	@JsonProperty("modified")
	private float modified;
	@JsonProperty("projects_read")
	private List<String> projectsRead;
	@JsonProperty("nsState")
	private String nsState;
	@JsonProperty("created")
	private float created;
	
	public DeploymentInfo getDeployed() {
		return deployed;
	}
	public void setDeployed(DeploymentInfo deployed) {
		this.deployed = deployed;
	}
	public List<String> getProjectsWrite() {
		return projectsWrite;
	}
	public void setProjectsWrite(List<String> projectsWrite) {
		this.projectsWrite = projectsWrite;
	}
	public String getNsLcmOp() {
		return nsLcmOp;
	}
	public void setNsLcmOp(String nsLcmOp) {
		this.nsLcmOp = nsLcmOp;
	}
	public float getModified() {
		return modified;
	}
	public void setModified(float modified) {
		this.modified = modified;
	}
	public List<String> getProjectsRead() {
		return projectsRead;
	}
	public void setProjectsRead(List<String> projectsRead) {
		this.projectsRead = projectsRead;
	}
	public String getNsState() {
		return nsState;
	}
	public void setNsState(String nsState) {
		this.nsState = nsState;
	}
	public float getCreated() {
		return created;
	}
	public void setCreated(float created) {
		this.created = created;
	}
}
