package edu.upc.gco.slcnt.rest.client.osm.lcm.nsiManagement.elements;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class CreateNSRequest {

	@JsonProperty("nsName")
	private String nsName;
	@JsonProperty("nsdId")
	private String nsdId;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("nsDescription")
	private String nsDescription;
	@JsonProperty("vimAccountId")
	private String vimAccountId;
	
	public String getNsName() {
		return nsName;
	}
	public void setNsName(String nsName) {
		this.nsName = nsName;
	}
	public String getNsdId() {
		return nsdId;
	}
	public void setNsdId(String nsdId) {
		this.nsdId = nsdId;
	}
	public String getVimAccountId() {
		return vimAccountId;
	}
	public void setVimAccountId(String vimAccountId) {
		this.vimAccountId = vimAccountId;
	}
	public String getNsDescription() {
		return nsDescription;
	}
	public void setNsDescription(String nsDescription) {
		this.nsDescription = nsDescription;
	}
	
	@Override
	public String toString() {
		return "CreateNSRequest [nsName=" + nsName + ", nsdId=" + nsdId + ", nsDescription=" + nsDescription
				+ ", vimAccountId=" + vimAccountId + "]";
	}
}
