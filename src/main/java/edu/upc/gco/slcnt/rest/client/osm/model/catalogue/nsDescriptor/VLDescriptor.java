package edu.upc.gco.slcnt.rest.client.osm.model.catalogue.nsDescriptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VLDescriptor {

	@JsonProperty("type")
	private String type;
	@JsonProperty("name")
	private String networkName;
	@JsonProperty("id")
	private String networkId;
	@JsonProperty("short-name")
	private String shortName;
	@JsonProperty("mngmt-network")
	private boolean mngmtNetwork;
	@JsonProperty("vnfd-connection-point-ref")
	private List<VNFDCPRefElement> vnfdCPs;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public boolean isMngmtNetwork() {
		return mngmtNetwork;
	}
	public void setMngmtNetwork(boolean mngmtNetwork) {
		this.mngmtNetwork = mngmtNetwork;
	}
	public List<VNFDCPRefElement> getVnfdCPs() {
		return vnfdCPs;
	}
	public void setVnfdCPs(List<VNFDCPRefElement> vnfdCPs) {
		this.vnfdCPs = vnfdCPs;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
    
	@Override
	public String toString() {
		return "VLDescriptor [type=" + type + ", networkName=" + networkName + ", networkId=" + networkId
				+ ", shortName=" + shortName + ", mngmtNetwork=" + mngmtNetwork + ", vnfdCPs=" + vnfdCPs
				+ ", otherProperties=" + otherProperties + "]";
	}
}
