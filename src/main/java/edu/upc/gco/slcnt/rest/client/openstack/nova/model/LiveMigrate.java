package edu.upc.gco.slcnt.rest.client.openstack.nova.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LiveMigrate {

	@JsonProperty("host")
	private String host = null;
	
	@JsonProperty("block_migration")
	private String blockMigration = null;
	
	@JsonProperty("disk_over_commit")
	private String diskOverCommit = null;
	
	public LiveMigrate (String host, String blockMigration, String diskOverCommit)
	{
		this.host = host;
		this.blockMigration = blockMigration;
		this.diskOverCommit = diskOverCommit;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getBlockMigration() {
		return blockMigration;
	}

	public void setBlockMigration(String blockMigration) {
		this.blockMigration = blockMigration;
	}

	public String getDiskOverCommit() {
		return diskOverCommit;
	}

	public void setDiskOverCommit(String diskOverCommit) {
		this.diskOverCommit = diskOverCommit;
	}
}
