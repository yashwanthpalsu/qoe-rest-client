package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.vnfi;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Interface {

	@JsonProperty("ns-vld-id")
	private String nsVldId;
	@JsonProperty("mac-address")
	private String macAddress;
	@JsonProperty("mgmt-vnf")
	private boolean mgmtVnf;
	@JsonProperty("name")
	private String name;
	@JsonProperty("ip-address")
	private String ipAddress;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public String getNsVldId() {
		return nsVldId;
	}

	public void setNsVldId(String nsVldId) {
		this.nsVldId = nsVldId;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public boolean isMgmtVnf() {
		return mgmtVnf;
	}

	public void setMgmtVnf(boolean mgmtVnf) {
		this.mgmtVnf = mgmtVnf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}
