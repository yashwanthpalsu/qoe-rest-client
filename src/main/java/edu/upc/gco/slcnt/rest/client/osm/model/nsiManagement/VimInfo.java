package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VimInfo {

	@JsonProperty("admin_state_up")
	private boolean adminStateUp;
	
	@JsonProperty("availability_zone_hints")
	private List<Object> availabilityZoneHints;
	
	@JsonProperty("availability_zones")
	private List<String> availabilityZones;
	
	@JsonProperty("created_at")
	private String createdAt;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("encapsulation")
	private String encapsulation;
	
	@JsonProperty("encapsulation_id")
	private String encapsulationId;
	
	@JsonProperty("encapsulation_type")
	private String encapsulationType;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("ipv4_address_scope")
	private String ipv4AddressScope;
	
	@JsonProperty("ipv6_address_scope")
	private String ipv6AddressScope;
	
	@JsonProperty("mtu")
	private int mtu;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("port_security_enabled")
	private boolean portSecurityEnabled;
	
	@JsonProperty("project_id")
	private String projectId;
	
	@JsonProperty("provider:network_type")
	private String providerNetworkType;
	
	@JsonProperty("provider:physical_network")
	private String providerPhysicalNetwork;
	
	@JsonProperty("provider:segmentation_id")
	private String providerSegmentationId;
	
	@JsonProperty("revision_number")
	private int revisionNumber;
	
	@JsonProperty("router:external")
	private boolean routerExternal;
	
	@JsonProperty("segmentation_id")
	private int segmentationId;
	
	@JsonProperty("shared")
	private boolean shared;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("subnets")
	private List<SubNet> subnets;
	
	@JsonProperty("tags")
	private List<String> tags;
	
	@JsonProperty("tenant_id")
	private String tenantId;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("updated_at")
	private String updatedAt;
	
	@JsonProperty("allowed_address_pairs")
	private List<String> allowedAddressPairs;
	
	@JsonProperty("binding:host_id")
	private String bindingHostId;
	
	@JsonProperty("binding:profile")
	private String bindingProfile;
	
	@JsonProperty("binding:vif_details")
	private VifDetails bindingVifDetails;
	
	@JsonProperty("binding:vif_type")
	private String bindingVifType;
	
	@JsonProperty("binding:vnic_type")
	private String bindingVnicType;
	
	@JsonProperty("device_id")
	private String deviceId;
	
	@JsonProperty("device_owner")
	private String deviceOwner;
	
	@JsonProperty("extra_dhcp_opts")
	private List<String> extraDhcpOpts;
	
	@JsonProperty("fixed_ips")
	private List<FixedIp> fixedIps;
	
	@JsonProperty("mac_address")
	private String macAddress;
	
	@JsonProperty("network_id")
	private String networkId;
	
	@JsonProperty("security_groups")
	private List<String> securityGroups;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public boolean isAdminStateUp() {
		return adminStateUp;
	}

	public void setAdminStateUp(boolean adminStateUp) {
		this.adminStateUp = adminStateUp;
	}

	public List<Object> getAvailabilityZoneHints() {
		return availabilityZoneHints;
	}

	public void setAvailabilityZoneHints(List<Object> availabilityZoneHints) {
		this.availabilityZoneHints = availabilityZoneHints;
	}

	public List<String> getAvailabilityZones() {
		return availabilityZones;
	}

	public void setAvailabilityZones(List<String> availabilityZones) {
		this.availabilityZones = availabilityZones;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEncapsulation() {
		return encapsulation;
	}

	public void setEncapsulation(String encapsulation) {
		this.encapsulation = encapsulation;
	}

	public String getEncapsulationId() {
		return encapsulationId;
	}

	public void setEncapsulationId(String encapsulationId) {
		this.encapsulationId = encapsulationId;
	}

	public String getEncapsulationType() {
		return encapsulationType;
	}

	public void setEncapsulationType(String encapsulationType) {
		this.encapsulationType = encapsulationType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIpv4AddressScope() {
		return ipv4AddressScope;
	}

	public void setIpv4AddressScope(String ipv4AddressScope) {
		this.ipv4AddressScope = ipv4AddressScope;
	}

	public String getIpv6AddressScope() {
		return ipv6AddressScope;
	}

	public void setIpv6AddressScope(String ipv6AddressScope) {
		this.ipv6AddressScope = ipv6AddressScope;
	}

	public int getMtu() {
		return mtu;
	}

	public void setMtu(int mtu) {
		this.mtu = mtu;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPortSecurityEnabled() {
		return portSecurityEnabled;
	}

	public void setPortSecurityEnabled(boolean portSecurityEnabled) {
		this.portSecurityEnabled = portSecurityEnabled;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProviderNetworkType() {
		return providerNetworkType;
	}

	public void setProviderNetworkType(String providerNetworkType) {
		this.providerNetworkType = providerNetworkType;
	}

	public String getProviderPhysicalNetwork() {
		return providerPhysicalNetwork;
	}

	public void setProviderPhysicalNetwork(String providerPhysicalNetwork) {
		this.providerPhysicalNetwork = providerPhysicalNetwork;
	}

	public String getProviderSegmentationId() {
		return providerSegmentationId;
	}

	public void setProviderSegmentationId(String providerSegmentationId) {
		this.providerSegmentationId = providerSegmentationId;
	}

	public int getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(int revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public boolean isRouterExternal() {
		return routerExternal;
	}

	public void setRouterExternal(boolean routerExternal) {
		this.routerExternal = routerExternal;
	}

	public int getSegmentationId() {
		return segmentationId;
	}

	public void setSegmentationId(int segmentationId) {
		this.segmentationId = segmentationId;
	}

	public boolean isShared() {
		return shared;
	}

	public void setShared(boolean shared) {
		this.shared = shared;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SubNet> getSubnets() {
		return subnets;
	}

	public void setSubnets(List<SubNet> subnets) {
		this.subnets = subnets;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<String> getAllowedAddressPairs() {
		return allowedAddressPairs;
	}

	public void setAllowedAddressPairs(List<String> allowedAddressPairs) {
		this.allowedAddressPairs = allowedAddressPairs;
	}

	public String getBindingHostId() {
		return bindingHostId;
	}

	public void setBindingHostId(String bindingHostId) {
		this.bindingHostId = bindingHostId;
	}

	public String getBindingProfile() {
		return bindingProfile;
	}

	public void setBindingProfile(String bindingProfile) {
		this.bindingProfile = bindingProfile;
	}

	public VifDetails getBindingVifDetails() {
		return bindingVifDetails;
	}

	public void setBindingVifDetails(VifDetails bindingVifDetails) {
		this.bindingVifDetails = bindingVifDetails;
	}

	public String getBindingVifType() {
		return bindingVifType;
	}

	public void setBindingVifType(String bindingVifType) {
		this.bindingVifType = bindingVifType;
	}

	public String getBindingVnicType() {
		return bindingVnicType;
	}

	public void setBindingVnicType(String bindingVnicType) {
		this.bindingVnicType = bindingVnicType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceOwner() {
		return deviceOwner;
	}

	public void setDeviceOwner(String deviceOwner) {
		this.deviceOwner = deviceOwner;
	}

	public List<String> getExtraDhcpOpts() {
		return extraDhcpOpts;
	}

	public void setExtraDhcpOpts(List<String> extraDhcpOpts) {
		this.extraDhcpOpts = extraDhcpOpts;
	}

	public List<FixedIp> getFixedIps() {
		return fixedIps;
	}

	public void setFixedIps(List<FixedIp> fixedIps) {
		this.fixedIps = fixedIps;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public List<String> getSecurityGroups() {
		return securityGroups;
	}

	public void setSecurityGroups(List<String> securityGroups) {
		this.securityGroups = securityGroups;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}
