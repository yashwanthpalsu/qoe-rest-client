package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchResult {

	@JsonProperty("validityPeriod")
	private int validityPeriod;
	@JsonProperty("cpsInstances")
	private List<CPSProfile> cpsInstances;
	
	public SearchResult ()
	{
		
	}
	
	public SearchResult (int validityPeriod)
	{
		this.validityPeriod = validityPeriod;
		this.cpsInstances = new ArrayList<CPSProfile>();
	}

	public int getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(int validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

	public List<CPSProfile> getCpsInstances() {
		return cpsInstances;
	}

	public void setCpsInstances(List<CPSProfile> cpsInstances) {
		this.cpsInstances = cpsInstances;
	}
}
