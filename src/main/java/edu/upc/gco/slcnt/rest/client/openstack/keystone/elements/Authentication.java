package edu.upc.gco.slcnt.rest.client.openstack.keystone.elements;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Authentication {

	@JsonProperty("identity")
	private Identity identity = null;
	
	@JsonProperty("scope")
	private Scope scope = null;
	
	public Authentication (Identity identity, Scope scope)
	{
		this.identity = identity;
		this.scope = scope;
	}

	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}
}
