package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import java.util.ArrayList;

public class PatchDocument extends ArrayList<PatchItem> {

	@Override
	public boolean equals (java.lang.Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		return super.equals(o);
	}
	
	@Override
	public String toString()
	{
		String str = "[";
		for (PatchItem p : this)
		{
			str += "{" + p.toString() + "}";
		}
		return str += "]";
	}
}
