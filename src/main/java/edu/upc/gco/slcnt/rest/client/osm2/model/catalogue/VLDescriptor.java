package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VLDescriptor {

	@JsonProperty("type")
	private String type;
	@JsonProperty("name")
	private String networkName;
	@JsonProperty("id")
	private String networkId;
	@JsonProperty("short-name")
	private String shortName;
	@JsonProperty("mngmt-network")
	private boolean mngmtNetwork;
	@JsonProperty("vnfd-connection-point-ref")
	private List<VnfDescriptorCPRefElement> vnfdCPs;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public boolean isMngmtNetwork() {
		return mngmtNetwork;
	}
	public void setMngmtNetwork(boolean mngmtNetwork) {
		this.mngmtNetwork = mngmtNetwork;
	}
	public List<VnfDescriptorCPRefElement> getVnfdCPs() {
		return vnfdCPs;
	}
	public void setVnfdCPs(List<VnfDescriptorCPRefElement> vnfdCPs) {
		this.vnfdCPs = vnfdCPs;
	}
}
