package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.nbi.elements;

import edu.upc.gco.slcnt.rest.client.common.interfaces.elements.EndPoint;

public class SdnEndPoint extends EndPoint {

	private String borderNodeId;
	private String portId;
	
	public SdnEndPoint ()
	{
		
	}
	
	public SdnEndPoint (String clientIpNetwork, String borderIpNetwork, String borderMacAddress, String borderNodeId, String portId)
	{
		super(clientIpNetwork, borderIpNetwork, borderMacAddress);
		this.borderNodeId = borderNodeId;
		this.portId = portId;
	}

	public String getBorderNodeId() {
		return borderNodeId;
	}

	public String getPortId() {
		return portId;
	}
	
	public void setPortId(String portId) {
		this.portId = portId;
	}
}
