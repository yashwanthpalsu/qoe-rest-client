package edu.upc.gco.slcnt.rest.client.common.interfaces.elements;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceInformationElement;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class EndPoint implements InterfaceInformationElement {

	@JsonProperty("client_ip_network")
	private String clientIpNetwork; //0.0.0.0/24
	@JsonProperty("border_ip_network")
	private String borderIpNetwork; //0.0.0.0/24
	@JsonProperty("border_mac_address")
	private String borderMacAddress; //00:00:00:00:00:01
	
	public EndPoint ()
	{
		
	}
	
	public EndPoint(String clientIpNetwork, String borderIpNetwork, String borderMacAddress) {

		this.clientIpNetwork = clientIpNetwork;
		this.borderIpNetwork = borderIpNetwork;
		this.borderMacAddress = borderMacAddress;
	}

	public String getClientIpNetwork() {
		return clientIpNetwork;
	}

	public void setClientIpNetwork(String clientIpNetwork) {
		this.clientIpNetwork = clientIpNetwork;
	}

	public String getBorderIpNetwork() {
		return borderIpNetwork;
	}

	public void setBorderIpNetwork(String borderIpNetwork) {
		this.borderIpNetwork = borderIpNetwork;
	}

	public String getBorderMacAddress() {
		return borderMacAddress;
	}

	public void setBorderMacAddress(String borderMacAddress) {
		this.borderMacAddress = borderMacAddress;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		// TODO Auto-generated method stub
		
	}

}
