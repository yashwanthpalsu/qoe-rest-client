package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.rest.client.Utils;
import edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements.GetPortNumberBody;
import edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements.Link;
import edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements.Node;
import edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements.OXCconfiguration;
import edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements.Port;
import edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements.Topology;

public class MininetApi {

	private Log logger = LogFactory.getLog(MininetApi.class);
	
	private String proto;
	private String ip;
	private String port;
	
	public MininetApi(String proto, String ip, String port) {
		
		this.proto = proto;
		this.ip = ip;
		this.port = port;
	}

	public Topology getTopology() {
		
		String uri = proto + "://" + ip + ":" + port +"/getEdges";
		logger.info("Initializing topology; URI = " + uri);
		
		Topology topology = new Topology();
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		
		HttpEntity<String> getEdges = new HttpEntity<String>(headers);
		ResponseEntity<String> re = rt.exchange(uri, HttpMethod.GET, getEdges, String.class);
		
		JsonNode edges = null;
		try {
			
			edges = new ObjectMapper().readTree(re.getBody());
			if(edges.isArray()) {
				
				for(int e = 0; e<edges.size(); e++) {
					
					String linkId = edges.get(e).get("linkId").asText();
					
					JsonNode attrs = edges.get(e).get("attrs");
					String srcNodeId = attrs.get("node1").asText();
					String dstNodeId = attrs.get("node2").asText();
					String srcPortId = attrs.get("port1").asText();
					String dstPortId = attrs.get("port2").asText();
					String bw = attrs.get("bw").asText();
					String delay = attrs.get("delay").asText();
					String wl = attrs.get("wl").asText();
					
					Node src, dst;
					
					if(!topology.getNodes().containsKey(srcNodeId)) {
						
						src = new Node(srcNodeId);
						topology.getNodes().put(srcNodeId, src);
						logger.info("Added node with nodeId = "+srcNodeId);
					}
					else {
						
						src = topology.getNodes().get(srcNodeId);
					}
					
					if(!topology.getNodes().containsKey(dstNodeId)) {
						
						dst = new Node(dstNodeId);
						topology.getNodes().put(dstNodeId, dst);
						logger.info("Added node with nodeId = "+dstNodeId);
					}
					else {
						
						dst = topology.getNodes().get(dstNodeId);
					}
				 
					Port srcPort = new Port(srcPortId);
					Port dstPort = new Port(dstPortId);
					src.addPort(srcPort);
					logger.info("Added port with portId = "+srcPortId+" at node with nodeID = "+srcNodeId);
					dst.addPort(dstPort);
					logger.info("Added port with portId = "+dstPortId+" at node with nodeID = "+dstNodeId);
					
					Link link = new Link(linkId, src, srcPort, dst, dstPort, bw, delay, wl);
					topology.getLinks().put(linkId, link);
					logger.info("Added link with linkId = "+linkId+" (src = "+srcNodeId+":"+srcPortId+", dst = "+dstNodeId+":"+dstPortId+", bw = "+bw+", delay = "+delay+", wl = "+wl+")");
					
					src.addOutLink(link);
					dst.addInLink(link);
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return topology;
	}
	
	public void configureOXC(OXCconfiguration oxc_configuration) {
		
		String uri = proto + "://" + ip + ":" + port +"/configOXC";
		logger.info("Configuring OXC; URI = " + uri);
		logger.info("OXC ID = "+oxc_configuration.getOxc()+", Operation = "+oxc_configuration.getCmd());
		logger.info("End-points: "+oxc_configuration.getEnd_points());
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> postEntity = new HttpEntity<>(oxc_configuration, header);
		rt.exchange(uri, HttpMethod.POST, postEntity, String.class);
	}
	
	public String getPortNumber(String node, String ifaceName) {
		
		String uri = proto + "://" + ip + ":" + port +"/getPortNumber";
		logger.info("Getting port number for interface "+ ifaceName + " at node "+node);
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		GetPortNumberBody requestBody = new GetPortNumberBody(node, ifaceName);
		HttpEntity<?> getEntity = new HttpEntity<>(requestBody, header);
		ResponseEntity<String> re = rt.exchange(uri, HttpMethod.POST, getEntity, String.class);
		
		return re.getBody();
	}
}