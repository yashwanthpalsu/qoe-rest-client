package edu.upc.gco.slcnt.rest.client.osm.lcm;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.rest.client.Utils;
import edu.upc.gco.slcnt.rest.client.osm.lcm.nsiManagement.NSIManagementInterface;
import edu.upc.gco.slcnt.rest.client.osm.lcm.nsiManagement.elements.CreateNSRequest;
import edu.upc.gco.slcnt.rest.client.osm.lcm.nsiManagement.elements.TerminateNSRequest;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.NSInstance;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.NSInstanceId;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.OperationOccurrence;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.vnfi.VNFInstance;
import edu.upc.gco.slcnt.rest.client.osm.model.osmManagement.ProjectObject;
import edu.upc.gco.slcnt.rest.client.osm.model.osmManagement.VimInfoObject;
import edu.upc.gco.slcnt.rest.client.osm.osmManagement.OSMManagementInterface;
import it.nextworks.nfvmano.libs.osmr4PlusClient.osmManagement.elements.Token;
import it.nextworks.nfvmano.libs.osmr4PlusClient.osmManagement.elements.TokenRequest;
import it.nextworks.nfvmano.libs.osmr4PlusClient.utilities.OSMHttpResponse;

public class OSMLcmClient implements NSIManagementInterface, OSMManagementInterface {

	private Log logger = LogFactory.getLog(OSMLcmClient.class);
	private RestTemplate restTemplate;
	private HttpComponentsClientHttpRequestFactory requestFactory;
	
	private String  osmIPAddress, user, password, project;
    private Token   validToken;
    
    final private String osmManagement = "/admin/v1";
    final private String nsiManagement = "/nslcm/v1";
    
    public OSMLcmClient (String osmIPAddress, String user, String password, String project)
    {
		this.osmIPAddress = osmIPAddress;
		this.user = user;
		this.password = password;
		this.project = project;
		
		requestFactory = Utils.getSslDisabledClientHttpRequestFactory();
		restTemplate = new RestTemplate(requestFactory);
		
		ResponseEntity<Token> response = (ResponseEntity<Token>) getAuthenticationToken();
		validToken = response.getBody();
    }
    
    protected String getOsmIPAddress() {
		return osmIPAddress;
	}

	protected String getUser() {
		return user;
	}

	protected String getPassword() {
		return password;
	}

	protected String getProject() {
		return project;
	}
    
    private void verifyToken()
    {
		Double tokenExpireTime = new Double (validToken.getExpires());
		if(tokenExpireTime.longValue()*1000 < System.currentTimeMillis())
        {
        	//validToken = getAuthenticationToken();
			ResponseEntity<Token> response = (ResponseEntity<Token>) getAuthenticationToken();
			validToken = response.getBody();
        }
    }
    
    //TODO: Refine and finish this Error Processing according to OSM OPENAPI
    private ResponseEntity<?> processHttpClientErrorException (int error)
	{
		switch (error)
		{
			case 404:
				logger.error ("404: Not Found");
				return new ResponseEntity<String>("Not Found", HttpStatus.NOT_FOUND);
			default:
				logger.error(error + ": Unsupported");
				return new ResponseEntity<String>("Unsupported Error", HttpStatus.valueOf(error));
		}
	}
    
    //public Token getAuthenticationToken()
    @Override
    public ResponseEntity<?> getAuthenticationToken()
	{
    	String url = "https://" + osmIPAddress + ":9999/osm" + osmManagement + "/tokens";
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		TokenRequest tokenReq = new TokenRequest(user, password, project);
		HttpEntity<TokenRequest> request = new HttpEntity<TokenRequest>(tokenReq, headers);
		ResponseEntity<Token> response = restTemplate.exchange(url, HttpMethod.POST, request, Token.class);
		//logger.info("STATUS: " + response.getStatusCodeValue());
		//logger.info("RESPONSE: " + response.getBody().toString());
		if (response.getStatusCode() == HttpStatus.UNAUTHORIZED)
		{
			throw new RuntimeException(response.getBody().toString());
		}
		
		return response;
	}
    /*
     public Token getAuthenticationToken()
	{
    	String url = "https://" + osmIPAddress + ":9999/osm" + osmManagement + "/tokens";
		
		//HttpComponentsClientHttpRequestFactory requestFactory = Utils.getSslDisabledClientHttpRequestFactory();
		//RestTemplate restTemplate = new RestTemplate(requestFactory);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		TokenRequest tokenReq = new TokenRequest(user, password, project);
		HttpEntity<TokenRequest> request = new HttpEntity<TokenRequest>(tokenReq, headers);
		ResponseEntity<Token> response = restTemplate.exchange(url, HttpMethod.POST, request, Token.class);
		//logger.info("STATUS: " + response.getStatusCodeValue());
		//logger.info("RESPONSE: " + response.getBody().toString());
		if (response.getStatusCode() == HttpStatus.UNAUTHORIZED)
		{
			throw new RuntimeException(response.getBody().toString());
		}
		
		return response.getBody();
	}
     */
    
	@Override
	public ResponseEntity<?> createNsiIdentifier(String nsName, String nsdId, String nsDescription, String vimAccountId) {
		
		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + nsiManagement + "/ns_instances";
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		CreateNSRequest nsiRequest = new CreateNSRequest();
		nsiRequest.setNsName(nsName);
		nsiRequest.setNsdId(nsdId);
		nsiRequest.setNsDescription(nsDescription);
		nsiRequest.setVimAccountId(vimAccountId);
		
		HttpEntity<CreateNSRequest> request = new HttpEntity<CreateNSRequest>(nsiRequest, headers);
		try
		{
			ResponseEntity<NSInstanceId> response = restTemplate.exchange(url, HttpMethod.POST, request, NSInstanceId.class);
			return response;
		}
		catch (HttpClientErrorException hcee)
		{
			return processHttpClientErrorException (hcee.getRawStatusCode());
		}
	}

	@Override
	public ResponseEntity<?> getNsiInfoList() {
		
		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + nsiManagement + "/ns_instances";
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		try
		{
			ResponseEntity<List<NSInstance>> response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<NSInstance>>(){});
			return response;
		}
		catch (HttpClientErrorException hcee)
		{
			return processHttpClientErrorException (hcee.getRawStatusCode());
		}
	}
	
	@Override
	public ResponseEntity<?> getNsiInfo(String nsiId) {
		
		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + nsiManagement + "/ns_instances/" + nsiId;
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		try
		{
			ResponseEntity<NSInstance> response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<NSInstance>(){});
			return response;
		}
		catch (HttpClientErrorException hcee)
		{
			return processHttpClientErrorException (hcee.getRawStatusCode());
		}
	}
	
	@Override
	public ResponseEntity<?> instantiateNsi(String nsiId, String nsName, String nsdId, String vimAccountId) {

		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + nsiManagement + "/ns_instances/" + nsiId + "/instantiate";
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		CreateNSRequest nsiRequest = new CreateNSRequest();
		nsiRequest.setNsName(nsName);
		nsiRequest.setNsdId(nsdId);
		nsiRequest.setVimAccountId(vimAccountId);
		
		HttpEntity<CreateNSRequest> request = new HttpEntity<CreateNSRequest>(nsiRequest, headers);
		try
		{
			ResponseEntity<OperationOccurrence> response = restTemplate.exchange(url, HttpMethod.POST, request, OperationOccurrence.class);
			return response;
		}
		catch (HttpClientErrorException hcee)
		{
			return processHttpClientErrorException (hcee.getRawStatusCode());
		}
	}

	@Override
	public ResponseEntity<?> teminateNsi(String nsiId, LocalDateTime terminationTime) {

		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + nsiManagement + "/ns_instances/" + nsiId + "/terminate";
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		TerminateNSRequest terminateRequest = new TerminateNSRequest();
		//terminateRequest.setTerminationTime(terminationTime);
		int timeOut = terminationTime.toLocalTime().compareTo(LocalTime.now());
		logger.info("Time Out set to = " + timeOut);
		if (timeOut <= 0)
			timeOut = 1;
		logger.info("Sending Termination with TimeOut = " + timeOut);
		terminateRequest.setTimeoutNsTerminate(timeOut);
		terminateRequest.setAutoremove(true);
		terminateRequest.setSkipTerminatePrimitives(true);
		
		HttpEntity<TerminateNSRequest> request = new HttpEntity<TerminateNSRequest>(terminateRequest, headers);
		
		try
		{
			ResponseEntity<NSInstanceId> response = restTemplate.exchange(url, HttpMethod.POST, request, NSInstanceId.class);
			return response;
		}
		catch (HttpClientErrorException hcee)
		{
			return processHttpClientErrorException (hcee.getRawStatusCode());
		}
	}

	@Override
	public ResponseEntity<?> deleteNsInstance(String nsiId) {
		
		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + nsiManagement + "/ns_instances/" + nsiId;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		
		try {
			ResponseEntity<Void> response = restTemplate.exchange(url, HttpMethod.DELETE, request, Void.class);
			return response;
		}
		catch (HttpClientErrorException hcee)
		{
			return processHttpClientErrorException (hcee.getRawStatusCode());
		}
	}

	@Override
	public ResponseEntity<?> getVimInfo(String vimAccountId) {
		
		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + osmManagement + "/vim_accounts/" + vimAccountId;
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<VimInfoObject> response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<VimInfoObject>(){});
		
		return response;
	}

	@Override
	public ResponseEntity<?> getVimInfoList() {
		
		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + osmManagement + "/vim_accounts";
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<List<VimInfoObject>> response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<VimInfoObject>>(){});
		
		return response;
	}

	@Override
	public ResponseEntity<?> getProjects() {
		
		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + osmManagement + "/projects";
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<List<ProjectObject>> response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<ProjectObject>>(){});
		
		return response;
	}

	@Override
	public ResponseEntity<?> getVnfiInfoList() {
		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + nsiManagement + "/vnf_instances";
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<List<VNFInstance>> response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<VNFInstance>>(){});
		
		return response;
	}

	@Override
	public ResponseEntity<?> getVnfiInfo(String vnfiId) {
		verifyToken();
		
		String url = "https://" + this.getOsmIPAddress() + ":9999/osm" + nsiManagement + "/vnf_instances/" + vnfiId;
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + validToken.getId());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<VNFInstance> response = restTemplate.exchange(url, HttpMethod.GET, request, VNFInstance.class);
		
		return response;
	}
}
