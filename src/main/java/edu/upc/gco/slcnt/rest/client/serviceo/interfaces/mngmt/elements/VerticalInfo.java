package edu.upc.gco.slcnt.rest.client.serviceo.interfaces.mngmt.elements;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VerticalInfo {

	@JsonProperty("id")
	private String verticalId;
	
	@JsonProperty("name")
	private String verticalName;
	
	public VerticalInfo()
	{
		
	}
	
	public VerticalInfo (String id, String name)
	{
		this.verticalId = id;
		this.verticalName = name;
	}

	public String getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(String verticalId) {
		this.verticalId = verticalId;
	}

	public String getVerticalName() {
		return verticalName;
	}

	public void setVerticalName(String verticalName) {
		this.verticalName = verticalName;
	}
}
