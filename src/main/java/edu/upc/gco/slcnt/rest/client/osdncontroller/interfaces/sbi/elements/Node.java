package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements;

import java.util.ArrayList;
import java.util.List;

public class Node {

	private String nodeId;
	private List<Port> ports;
	private List<Link> inLinks;
	private List<Link> outLinks;
	
	public Node(String nodeId) {
		
		this.nodeId = nodeId;
		this.ports = new ArrayList<Port>();
		this.inLinks = new ArrayList<Link>();
		this.outLinks = new ArrayList<Link>();
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public List<Port> getPorts() {
		return ports;
	}

	public void setPorts(List<Port> ports) {
		this.ports = ports;
	}
	
	public void addPort(Port port) {
		ports.add(port);
	}

	public List<Link> getInLinks() {
		return inLinks;
	}

	public void setInlinks(List<Link> links) {
		this.inLinks = links;
	}
	
	public void addInLink(Link link) {
		inLinks.add(link);
	}
	
	public List<Link> getOutLinks() {
		return outLinks;
	}

	public void setOutlinks(List<Link> links) {
		this.outLinks = links;
	}
	
	public void addOutLink(Link link) {
		outLinks.add(link);
	}
}