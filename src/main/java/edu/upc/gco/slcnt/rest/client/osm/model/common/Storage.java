package edu.upc.gco.slcnt.rest.client.osm.model.common;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Storage {

	@JsonProperty("folder")
	private String folder;				// == NSD Id
	@JsonProperty("path")
	private String path;
	@JsonProperty("descriptor")
	private String descriptor;
	@JsonProperty("pkg-dir")
	private String pkgDir;
	@JsonProperty("zipfile")
	private String zipFile;
	@JsonProperty("fs")
	private String fs;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}
	public String getPkgDir() {
		return pkgDir;
	}
	public void setPkgDir(String pkgDir) {
		this.pkgDir = pkgDir;
	}
	public String getZipFile() {
		return zipFile;
	}
	public void setZipFile(String zipFile) {
		this.zipFile = zipFile;
	}
	public String getFs() {
		return fs;
	}
	public void setFs(String fs) {
		this.fs = fs;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
    
	@Override
	public String toString() {
		return "Storage [folder=" + folder + ", path=" + path + ", descriptor=" + descriptor + ", pkgDir=" + pkgDir
				+ ", zipFile=" + zipFile + ", fs=" + fs + ", otherProperties=" + otherProperties + "]";
	}
}
