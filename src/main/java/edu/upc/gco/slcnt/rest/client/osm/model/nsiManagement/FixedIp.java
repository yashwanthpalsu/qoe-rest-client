package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FixedIp {

	@JsonProperty("ip_address")
	private String ipAddress;
	
	@JsonProperty("subnet_id")
	private String subnetId;

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getSubnetId() {
		return subnetId;
	}

	public void setSubnetId(String subnetId) {
		this.subnetId = subnetId;
	}
}
