package edu.upc.gco.slcnt.rest.client.osm2.model.inventory;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VLDescriptor {

	@JsonProperty("status-detailed")
	private String statusDetailed;	//TODO: Check type
	@JsonProperty("vim-id")
	private String vimId;
	@JsonProperty("status")
	private String status;
	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("vim-network-name")
	private String vimNetworkName;	// == name??
	
	public String getStatusDetailed() {
		return statusDetailed;
	}
	public void setStatusDetailed(String statusDetailed) {
		this.statusDetailed = statusDetailed;
	}
	public String getVimId() {
		return vimId;
	}
	public void setVimId(String vimId) {
		this.vimId = vimId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVimNetworkName() {
		return vimNetworkName;
	}
	public void setVimNetworkName(String vimNetworkName) {
		this.vimNetworkName = vimNetworkName;
	}
}
