package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VirtualInterface {

	@JsonProperty("type")
	private String type;
	@JsonProperty("bandwidth")
	private String bandwidth;
	@JsonProperty("vpci")
	private String vpci;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}
	public String getVpci() {
		return vpci;
	}
	public void setVpci(String vpci) {
		this.vpci = vpci;
	}
}
