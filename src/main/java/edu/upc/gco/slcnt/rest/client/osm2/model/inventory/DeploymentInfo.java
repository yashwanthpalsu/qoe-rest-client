package edu.upc.gco.slcnt.rest.client.osm2.model.inventory;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeploymentInfo {

	@JsonProperty("RO")
	private ResourceObject ro;
	@JsonProperty("VCA")
	private List vcaList;
	
	public ResourceObject getRo() {
		return ro;
	}
	public void setRo(ResourceObject ro) {
		this.ro = ro;
	}
	public List getVcaList() {
		return vcaList;
	}
	public void setVcaList(List vcaList) {
		this.vcaList = vcaList;
	}
}
