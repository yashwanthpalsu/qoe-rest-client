package edu.upc.gco.slcnt.rest.client.sliceo.interfaces.catalog.messages;

import java.util.List;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class CreateSliceTemplateRequest implements InterfaceMessage {

	private String tenantId;
	private String sliceType;
	private List<String> nsdList;
	
	public CreateSliceTemplateRequest ()
	{
		
	}
	
	public CreateSliceTemplateRequest (String tenantId, String sliceType, List<String> nsdList)
	{
		this.tenantId = tenantId;
		this.sliceType = sliceType;
		this.nsdList = nsdList;
	}
	
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getSliceType() {
		return sliceType;
	}

	public void setSliceType(String sliceType) {
		this.sliceType = sliceType;
	}

	public List<String> getNsdList() {
		return nsdList;
	}

	public void setNsdList(List<String> nsdList) {
		this.nsdList = nsdList;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (tenantId == null ) throw new MalformattedElementException("Create Slice Template request without Tenant ID");
		if (sliceType == null) throw new MalformattedElementException("Create Slice Template request without Slice Type");
		//if (nsdList == null) throw new MalformattedElementException("Create Slice Template request without NSDs");
	}

}
