package edu.upc.gco.slcnt.rest.client.osm2.model.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VnfDescriptorRefElement {

	@JsonProperty("member-vnf-index")
	private int vnfIndex;
	@JsonProperty("vnfd-id-ref")
	private String vnfdName;
	
	public int getVnfIndex() {
		return vnfIndex;
	}
	public void setVnfIndex(int vnfIndex) {
		this.vnfIndex = vnfIndex;
	}
	public String getVnfdName() {
		return vnfdName;
	}
	public void setVnfdName(String vnfdName) {
		this.vnfdName = vnfdName;
	}
}
