package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubNetObject {

	@JsonProperty("allocation_pools")
	private List<AllocationPool> allocationPools;
	
	@JsonProperty("cidr")
	private String cidr;
	
	@JsonProperty("created_at")
	private String createdAt;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("dns_nameservers")
	private List<String> dnsNameservers;
	
	@JsonProperty("enable_dhcp")
	private boolean enableDhcp;
	
	@JsonProperty("gateway_ip")
	private String gatewayIp;
	
	@JsonProperty("host_routes")
	private List<String> hostRoutes;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("ip_version")
	private int ipVersion;
	
	@JsonProperty("ipv6_address_mode")
	private String ipv6AddressMode;
	
	@JsonProperty("ipv6_ra_mode")
	private String ipv6RaMode;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("network_id")
	private String networkId;
	
	@JsonProperty("project_id")
	private String projectId;
	
	@JsonProperty("revision_number")
	private int revisionNumber;
	
	@JsonProperty("service_types")
	private List<String> serviceTypes;
	
	@JsonProperty("subnetpool_id")
	private String subnetpoolId;
	
	@JsonProperty("tags")
	private List<String>tags;
	
	@JsonProperty("tenant_id")
	private String tenantId;
	
	@JsonProperty("updated_at")
	private String updatedAt;

	public List<AllocationPool> getAllocationPools() {
		return allocationPools;
	}

	public void setAllocationPools(List<AllocationPool> allocationPools) {
		this.allocationPools = allocationPools;
	}

	public String getCidr() {
		return cidr;
	}

	public void setCidr(String cidr) {
		this.cidr = cidr;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getDnsNameservers() {
		return dnsNameservers;
	}

	public void setDnsNameservers(List<String> dnsNameservers) {
		this.dnsNameservers = dnsNameservers;
	}

	public boolean isEnableDhcp() {
		return enableDhcp;
	}

	public void setEnableDhcp(boolean enableDhcp) {
		this.enableDhcp = enableDhcp;
	}

	public String getGatewayIp() {
		return gatewayIp;
	}

	public void setGatewayIp(String gatewayIp) {
		this.gatewayIp = gatewayIp;
	}

	public List<String> getHostRoutes() {
		return hostRoutes;
	}

	public void setHostRoutes(List<String> hostRoutes) {
		this.hostRoutes = hostRoutes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(int ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getIpv6AddressMode() {
		return ipv6AddressMode;
	}

	public void setIpv6AddressMode(String ipv6AddressMode) {
		this.ipv6AddressMode = ipv6AddressMode;
	}

	public String getIpv6RaMode() {
		return ipv6RaMode;
	}

	public void setIpv6RaMode(String ipv6RaMode) {
		this.ipv6RaMode = ipv6RaMode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public int getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(int revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public List<String> getServiceTypes() {
		return serviceTypes;
	}

	public void setServiceTypes(List<String> serviceTypes) {
		this.serviceTypes = serviceTypes;
	}

	public String getSubnetpoolId() {
		return subnetpoolId;
	}

	public void setSubnetpoolId(String subnetpoolId) {
		this.subnetpoolId = subnetpoolId;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
