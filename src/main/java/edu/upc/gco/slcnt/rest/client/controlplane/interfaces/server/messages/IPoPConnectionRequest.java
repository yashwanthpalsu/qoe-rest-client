package edu.upc.gco.slcnt.rest.client.controlplane.interfaces.server.messages;

import java.util.HashMap;
import java.util.List;

import edu.upc.gco.slcnt.rest.client.common.interfaces.elements.EndPointPair;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class IPoPConnectionRequest implements InterfaceMessage {

	private List<EndPointPair> forwardingGraph;
	private HashMap<String,String> parameters;
	
	public IPoPConnectionRequest()
	{
		
	}
	
	public IPoPConnectionRequest (List<EndPointPair> forwardingGraph, HashMap<String,String> parameters)
	{
		this.forwardingGraph = forwardingGraph;
		this.parameters = parameters;
	}
	
	public List<EndPointPair> getForwardingGraph() {
		return forwardingGraph;
	}

	public HashMap<String, String> getParameters() {
		return parameters;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (forwardingGraph == null) throw new MalformattedElementException("Create Inter-PoP Connection without forwarding graph");
	}

}
