package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MgmtInterface {

	@JsonProperty("vdu-id")
	private String vduId;

	public String getVduId() {
		return vduId;
	}

	public void setVduId(String vduId) {
		this.vduId = vduId;
	}
}
