package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This object corresponds to the "_admin" information associated to the NSD.
 * 
 * @author fernando
 *
 */
public class AdminInfo {

	@JsonProperty("type")
	private String type;
	@JsonProperty("projects_write")
	private List<String> projectsWrite;				// OSM Project Ids
	@JsonProperty("onboardingState")
	private String onboardingState;					// Example value: ONBOARDED
	@JsonProperty("operationalState")
	private String operationalState;				// Example value: ENABLED
	@JsonProperty("projects_read")
	private List<String> projectsRead;
	@JsonProperty("modified")
	private float modified;
	@JsonProperty("storage")
	private Storage storage;
	@JsonProperty("usageState")
	private String usageState;						// Example value: NOT_IN_USE
	@JsonProperty("userDefinedData")
	private KeyValuePairs userDefinedData;
	@JsonProperty("created")
	private float created;
	
	public List<String> getProjectsWrite() {
		return projectsWrite;
	}
	public void setProjectsWrite(List<String> projectsWrite) {
		this.projectsWrite = projectsWrite;
	}
	public String getOnboardingState() {
		return onboardingState;
	}
	public void setOnboardingState(String onboardingState) {
		this.onboardingState = onboardingState;
	}
	public String getOperationalState() {
		return operationalState;
	}
	public void setOperationalState(String operationalState) {
		this.operationalState = operationalState;
	}
	public List<String> getProjectsRead() {
		return projectsRead;
	}
	public void setProjectsRead(List<String> projectsRead) {
		this.projectsRead = projectsRead;
	}
	public float getModified() {
		return modified;
	}
	public void setModified(float modified) {
		this.modified = modified;
	}
	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public String getUsageState() {
		return usageState;
	}
	public void setUsageState(String usageState) {
		this.usageState = usageState;
	}
	public KeyValuePairs getUserDefinedData() {
		return userDefinedData;
	}
	public void setUserDefinedData(KeyValuePairs userDefinedData) {
		this.userDefinedData = userDefinedData;
	}
	public float getCreated() {
		return created;
	}
	public void setCreated(float created) {
		this.created = created;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
