package edu.upc.gco.slcnt.rest.client.serviceo.interfaces.mngmt.messages;

import java.util.ArrayList;
import java.util.List;

import edu.upc.gco.slcnt.rest.client.serviceo.interfaces.mngmt.elements.VerticalInfo;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class GetVerticalsResponse implements InterfaceMessage {

	private List<VerticalInfo> verticals = new ArrayList<VerticalInfo>();
	
	public GetVerticalsResponse (List<VerticalInfo> verticals)
	{
		this.verticals = verticals;
	}
	
	public List<VerticalInfo> getVerticals()
	{
		return verticals;
	}
	
	@Override
	public void isValid() throws MalformattedElementException {
		// TODO Auto-generated method stub
		
	}

}
