package edu.upc.gco.slcnt.rest.client.openstack.keystone.elements;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.rest.client.openstack.model.keystone.User;

public class Password {

	@JsonProperty("user")
	private User user = null;
	
	public Password (User user)
	{
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
