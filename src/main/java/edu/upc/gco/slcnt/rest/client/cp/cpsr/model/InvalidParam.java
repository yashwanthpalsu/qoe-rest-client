package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvalidParam {

	@JsonProperty("param")
	private String parameter;
	@JsonProperty("reason")
	private String reason;
	
	public InvalidParam (String parameter, String reason)
	{
		this.parameter = parameter;
		this.reason = reason;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public String toString()
	{
		return "Invalid Parameter: " + this.parameter + " -> " + this.reason;
	}
}
