package edu.upc.gco.slcnt.rest.client.osm2;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.rest.client.Utils;
import edu.upc.gco.slcnt.rest.client.osm2.model.auth.Credentials;
import edu.upc.gco.slcnt.rest.client.osm2.model.auth.Token;
import edu.upc.gco.slcnt.rest.client.osm2.model.catalogue.NSDescriptor;
import edu.upc.gco.slcnt.rest.client.osm2.model.catalogue.VNFDescriptor;
import edu.upc.gco.slcnt.rest.client.osm2.model.inventory.NSInstance;

public class OsmApi {

	private Log logger = LogFactory.getLog(OsmApi.class);
	private String osmIP;
	private String osmPort;
	private String username;
	private String password;
	private String projectName;
	
	//local
	private String baseUrl;
	private Token token;
	
	public OsmApi()
	{
		
	}
	
	
	public OsmApi(String osmIP, String osmPort, String username, String password, String projectName) {
		this.osmIP = osmIP;
		this.osmPort = osmPort;
		this.username = username;
		this.password = password;
		this.projectName = projectName;
		
		this.baseUrl = "https://" + this.osmIP + ":" + this.osmPort;
	}


	// Authorization and Mangement API
	
	public String getAdminToken(/*String url, String adminUser, String adminPwd, String projectName*/)
	{
		String url = this.baseUrl + "/osm/admin/v1/tokens";
		
		HttpComponentsClientHttpRequestFactory requestFactory = Utils.getSslDisabledClientHttpRequestFactory();
		RestTemplate rt = new RestTemplate(requestFactory);
		String tokenStr = null;
		
		logger.info("URL: " + url + " -- Credentials: " + this.username + "/" + this.password + " / " + this.projectName);
		//RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Credentials credentials = new Credentials(this.username, this.password, this.projectName);
		HttpEntity<Credentials> request = new HttpEntity<Credentials>(credentials, headers);
		ResponseEntity<Token> re = rt.exchange(url, HttpMethod.POST, request, Token.class);
		logger.info("STATUS: " + re.getStatusCodeValue());
		logger.info("RESPONSE: " + re.getBody().toString());
		if (re.getStatusCode() == HttpStatus.OK)
		{
			this.token = re.getBody();
			tokenStr = ""+ this.token.getId();
		}
		
		return tokenStr;
	}
	
	public void listTokens ()
	{
		String url = this.baseUrl + "/osm/admin/v1/tokens";
		
		HttpComponentsClientHttpRequestFactory requestFactory = Utils.getSslDisabledClientHttpRequestFactory();
		RestTemplate rt = new RestTemplate(requestFactory);
		
		HttpHeaders headers = new HttpHeaders();
		//headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + this.token.getId());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<String> re = rt.exchange(url, HttpMethod.GET, request, String.class);
		logger.info("STATUS: " + re.getStatusCodeValue());
		logger.info("RESPONSE: " + re.getBody());
	}
	
	// Catalogue access API
	
	public void listNSDescriptors ()
	{
		String url = this.baseUrl + "/osm/nsd/v1/ns_descriptors";
		
		HttpComponentsClientHttpRequestFactory requestFactory = Utils.getSslDisabledClientHttpRequestFactory();
		RestTemplate rt = new RestTemplate(requestFactory);
		
		HttpHeaders headers = new HttpHeaders();
		//headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + this.token.getId());
		
		List<NSDescriptor> nsdList;
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<List<NSDescriptor>> re = rt.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<NSDescriptor>>(){});
		logger.info("STATUS: " + re.getStatusCodeValue());
		//logger.info("RESPONSE: " + re.getBody());
		nsdList = (List<NSDescriptor>) re.getBody();
		for (NSDescriptor nsd : nsdList)
		{
			logger.info("RESPONSE: " + nsd.getName());
		}
	}
	
	public void listVNFDescriptors ()
	{
		String url = this.baseUrl + "/osm/vnfpkgm/v1/vnf_packages";
		
		HttpComponentsClientHttpRequestFactory requestFactory = Utils.getSslDisabledClientHttpRequestFactory();
		RestTemplate rt = new RestTemplate(requestFactory);
		
		HttpHeaders headers = new HttpHeaders();
		//headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + this.token.getId());
		
		List<VNFDescriptor> vnfdList;
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<List<VNFDescriptor>> re = rt.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<VNFDescriptor>>(){});
		logger.info("STATUS: " + re.getStatusCodeValue());
		//logger.info("RESPONSE: " + re.getBody());
		vnfdList = (List<VNFDescriptor>) re.getBody();
		for (VNFDescriptor vnfd : vnfdList)
		{
			logger.info("RESPONSE: " + vnfd.getName());
		}
	}
	
	// Inventory access API
	
	public void listNSInstances ()
	{
		String url = this.baseUrl + "/osm/nslcm/v1/ns_instances";
		
		HttpComponentsClientHttpRequestFactory requestFactory = Utils.getSslDisabledClientHttpRequestFactory();
		RestTemplate rt = new RestTemplate(requestFactory);
		
		HttpHeaders headers = new HttpHeaders();
		//headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> acceptedMTList = new ArrayList<MediaType>();
		acceptedMTList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptedMTList);
		headers.set("Authorization", "Bearer " + this.token.getId());
		
		List<NSInstance> nsiList;
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<List<NSInstance>> re = rt.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<NSInstance>>(){});
		logger.info("STATUS: " + re.getStatusCodeValue());
		//logger.info("RESPONSE: " + re.getBody());
		nsiList = (List<NSInstance>) re.getBody();
		for (NSInstance nsi : nsiList)
		{
			logger.info("RESPONSE: " + nsi.getName());
		}
	}
}
