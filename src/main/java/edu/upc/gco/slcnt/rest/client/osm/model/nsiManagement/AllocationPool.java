package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AllocationPool {

	@JsonProperty("end")
	private String end;
	
	@JsonProperty("start")
	private String start;

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}
}
