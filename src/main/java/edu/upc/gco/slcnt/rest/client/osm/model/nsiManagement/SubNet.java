package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubNet {

	@JsonProperty("subnet")
	private SubNetObject subnet;

	public SubNetObject getSubnet() {
		return subnet;
	}

	public void setSubnet(SubNetObject subnet) {
		this.subnet = subnet;
	}
}
