package edu.upc.gco.slcnt.rest.client.qoe;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.rest.client.Utils;
import edu.upc.gco.slcnt.rest.client.qoe.model.QoEFeedback;

public class QoEOApi {

	private Log logger = LogFactory.getLog(QoEOApi.class);
	
	public void sendQoEFeedback (String url, QoEFeedback qoeObject)
	{
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	
		HttpEntity<QoEFeedback> qoeUpdate = new HttpEntity<QoEFeedback>(qoeObject, headers);
		logger.debug("PUT: " + url);
		rt.exchange(url, HttpMethod.PUT, qoeUpdate, Void.class);
	}
}
