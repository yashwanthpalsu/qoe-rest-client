package edu.upc.gco.slcnt.rest.client.osm;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.rest.client.osm.lcm.nsiManagement.NSIManagementInterface;
import it.nextworks.nfvmano.libs.osmr4PlusClient.OSMr4PlusClient;
import it.nextworks.nfvmano.libs.osmr4PlusClient.osmManagement.elements.Token;
import it.nextworks.nfvmano.libs.osmr4PlusClient.utilities.OSMHttpConnection;
import it.nextworks.nfvmano.libs.osmr4PlusClient.utilities.OSMHttpResponse;

public class OSMr4PlusClientExtended extends OSMr4PlusClient /*implements NSIManagementInterface*/ {

	final private String nsiManagement = "/nslcm/v1";
	
	public OSMr4PlusClientExtended(String osmIPAddress, String osmPort, String user, String password, String project) {
		super(osmIPAddress, osmPort, user, password, project);
		// TODO Auto-generated constructor stub
	}
    /*
	@Override
	public OSMHttpResponse createNsi() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OSMHttpResponse getNsiInfoList() {
		verifyToken();

        List<Header> headers = new ArrayList<>();
        headers.add(new BasicHeader("Accept", "application/json"));
        headers.add(new BasicHeader("Authorization", "Bearer " + validToken.getId()));

        String url = "https://" + this.getOSMIPAddress() + ":9999/osm" + nsiManagement + "/ns_instances";
        return OSMHttpConnection.establishOSMHttpConnection(url, OSMHttpConnection.OSMHttpMethod.GET, headers, null, null);
	}
	*/
}
