package edu.upc.gco.slcnt.rest.client.openstack;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.rest.client.Utils;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.KeystoneInterface;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.elements.AuthRequest;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.elements.Authentication;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.elements.Identity;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.elements.Password;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.elements.Scope;
import edu.upc.gco.slcnt.rest.client.openstack.keystone.elements.TokenRequest;
import edu.upc.gco.slcnt.rest.client.openstack.model.keystone.Domain;
import edu.upc.gco.slcnt.rest.client.openstack.model.keystone.Project;
import edu.upc.gco.slcnt.rest.client.openstack.model.keystone.Token;
import edu.upc.gco.slcnt.rest.client.openstack.model.keystone.User;
import edu.upc.gco.slcnt.rest.client.openstack.nova.NovaInterface;

public class OpenStackClient implements KeystoneInterface, NovaInterface {

	private RestTemplate restTemplate;
	private ClientHttpRequestFactory requestFactory;
	private boolean isAdmin;							// Differentiate between Admin and Regular clients
	
	private String keystoneIPAddress, keystonePort;
	private String novaIPAddress, novaPort;
	private String userName, userPwd, projectName, domainName;				// project == tenant
	private Token unscopedToken, validToken;
	
	private String userId, projectId;					// projectId needed for re-scaling and migrating
	
	private final String authManagement = "/auth/tokens";
	private final String userManagement = "/users";
	
	public OpenStackClient ()
	{
		requestFactory = Utils.getClientHttpRequestFactory();
		restTemplate = new RestTemplate(requestFactory);
		
		ResponseEntity<Token> response = (ResponseEntity<Token>) getUnscopedToken();
		unscopedToken = response.getBody();
		unscopedToken.setTokenStr(response.getHeaders().get("X-Subject-Token").get(0));
		
		response = (ResponseEntity<Token>) createProjectToken();
		validToken = response.getBody();
		validToken.setTokenStr(response.getHeaders().get("X-Subject-Token").get(0));
	}
	
	private void verifyToken()
	{
		ZoneId zone = ZoneId.systemDefault();
		long validTokenExpires = validToken.getExpiresAt().toEpochSecond(zone.getRules().getOffset(LocalDateTime.now()));
		if (validTokenExpires*1000 < System.currentTimeMillis())
		{
			// Renew Scoped Token
			ResponseEntity<Token> response = (ResponseEntity<Token>) getProjectToken();
			validToken = response.getBody();
			validToken.setTokenStr(response.getHeaders().get("X-Subject-Token").get(0));
		}
	}
	
	private void setUserId ()
	{
		ResponseEntity<List<User>> response = (ResponseEntity<List<User>>) getUsers();
		for (User u : response.getBody())
		{
			if (u.getName().equals(userName))
			{
				userId = u.getId();
				break;
			}
		}
	}
	
	private void setProjectId ()
	{
		ResponseEntity<List<Project>> response = (ResponseEntity<List<Project>>) getProjects();
		for (Project p : response.getBody())
		{
			if (p.getName().equals(projectName))
			{
				projectId = p.getId();
				break;
			}
		}
	}
	
	@Override
	public ResponseEntity<?> getUnscopedToken() {
		
		String url = "https://" + keystoneIPAddress + ":" + keystonePort + "/v3" + authManagement;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Domain domain = new Domain(domainName);
		User user = new User(userName, domain, userPwd);
		Password pwd = new Password(user);
		ArrayList<String> methods = new ArrayList<String>();
		methods.add("password");
		Identity identity = new Identity(methods, pwd, null);
		Authentication auth = new Authentication(identity, null);
		AuthRequest authReq = new AuthRequest(auth);
		
		HttpEntity<AuthRequest> request = new HttpEntity<AuthRequest>(authReq, headers);
		ResponseEntity<Token> response = restTemplate.exchange(url, HttpMethod.POST, request, Token.class);
		
		return response;
	}

	@Override
	public ResponseEntity<?> createProjectToken() {
		
		String url = "https://" + keystoneIPAddress + ":" + keystonePort + "/v3" + authManagement;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		TokenRequest usToken = new TokenRequest (unscopedToken.getTokenStr());
		ArrayList<String> methods = new ArrayList<String>();
		methods.add("token");
		Identity identity = new Identity(methods, null, usToken);
		Domain domain = new Domain(domainName);
		Project project = new Project(null, domain, projectName);
		Scope scope = new Scope(project);
		Authentication auth = new Authentication(identity, scope);
		AuthRequest authReq = new AuthRequest(auth);
		
		HttpEntity<AuthRequest> request = new HttpEntity<AuthRequest>(authReq, headers);
		ResponseEntity<Token> response = restTemplate.exchange(url, HttpMethod.POST, request, Token.class);
		
		return response;
	}
	
	@Override
	public ResponseEntity<?> getProjectToken() {
		
		String url = "https://" + keystoneIPAddress + ":" + keystonePort + "/v3" + authManagement;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		User user = new User(userId, userPwd);
		Password pwd = new Password(user);
		ArrayList<String> methods = new ArrayList<String>();
		methods.add("password");
		Identity identity = new Identity(methods, pwd, null);
		Project project = new Project(projectId, null, null);
		Scope scope = new Scope(project);
		Authentication auth = new Authentication(identity, scope);
		AuthRequest authReq = new AuthRequest(auth);
		
		HttpEntity<AuthRequest> request = new HttpEntity<AuthRequest>(authReq, headers);
		ResponseEntity<Token> response = restTemplate.exchange(url, HttpMethod.POST, request, Token.class);
		
		return response;
	}

	@Override
	public ResponseEntity<?> getUsers() {
		
		verifyToken();
		
		String url = "https://" + keystoneIPAddress + ":" + keystonePort + "/v3" + userManagement;
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Auth-Token", validToken.getTokenStr());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<List<User>> response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<User>>(){});
		
		return response;
	}

	@Override
	public ResponseEntity<?> getProjects() {
		
		verifyToken();
		
		String url = "https://" + keystoneIPAddress + ":" + keystonePort + "/v3" + authManagement + "/" + userId + "/projects";
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Auth-Token", validToken.getTokenStr());
		
		HttpEntity<Void> request = new HttpEntity<Void>(headers);
		ResponseEntity<List<Project>> response = restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<Project>>(){});
		
		return response;
	}

	@Override
	public ResponseEntity<?> getFlavors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> getResizeStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void liveMigrate() {
		// TODO Auto-generated method stub
		
	}
}
