package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PatchItem {

	/*
	 * Gets or Sets op
	 */
	public enum OpEnum {
		add("add"),
		remove("remove"),
		replace("replace"),
		move("move"),
		copy("copy"),
		test("test");
		
		private String value;
		
		OpEnum(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
		
		@Override
		public String toString() {
			return String.valueOf(value);
		}
		
		public static OpEnum fromValue(String text) {
			for (OpEnum b : OpEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}
	
	@JsonProperty("op")
	private OpEnum op;
	@JsonProperty("path")
	private String path;
	@JsonProperty("value")
	private Object value;
	@JsonProperty("from")
	private String from;
	
	public OpEnum getOp() {
		return op;
	}
	
	public void setOp(OpEnum op) {
		this.op = op;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public String getFrom() {
		return from;
	}
	
	public void setFrom(String from) {
		this.from = from;
	}
	
	@Override
	public String toString()
	{
		return "op = " + this.op.getValue() + " / path = " + this.path + " / value = " + this.getValue().toString() + " / from = " +this.from;
	}
}
