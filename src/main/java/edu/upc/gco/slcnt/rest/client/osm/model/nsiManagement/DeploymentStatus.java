package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeploymentStatus {

	@JsonProperty("classifications")
	private List<String> classifications;
	
	@JsonProperty("created_at")
	private String createdAt;
	
	@JsonProperty("datacenter_id")
	private String datacenterId;
	
	@JsonProperty("datacenter_tenant_id")
	private String datacenterTenantId;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("nets")
	private List<Net> nets;
	
	@JsonProperty("nsd_osm_id")
	private String nsdOsmId;
	
	@JsonProperty("scenario_id")
	private String scenarioId;
	
	@JsonProperty("scenario_name")
	private String scenarioName;
	
	@JsonProperty("sdn_nets")
	private List<SdnNet> sdnNets;
	
	@JsonProperty("sfis")
	private List<Object> sfis;
	
	@JsonProperty("sfps")
	private List<Object> sfps;
	
	@JsonProperty("sfs")
	private List<Object> sfs;
	
	@JsonProperty("tenant_id")
	private String tenantId;
	
	@JsonProperty("uuid")
	private String uuid;
	
	@JsonProperty("vnfs")
	private List<VNF> vnfs;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();

	public List<String> getClassifications() {
		return classifications;
	}

	public void setClassifications(List<String> classifications) {
		this.classifications = classifications;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getDatacenterId() {
		return datacenterId;
	}

	public void setDatacenterId(String datacenterId) {
		this.datacenterId = datacenterId;
	}

	public String getDatacenterTenantId() {
		return datacenterTenantId;
	}

	public void setDatacenterTenantId(String datacenterTenantId) {
		this.datacenterTenantId = datacenterTenantId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Net> getNets() {
		return nets;
	}

	public void setNets(List<Net> nets) {
		this.nets = nets;
	}

	public String getNsdOsmId() {
		return nsdOsmId;
	}

	public void setNsdOsmId(String nsdOsmId) {
		this.nsdOsmId = nsdOsmId;
	}

	public String getScenarioId() {
		return scenarioId;
	}

	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}

	public String getScenarioName() {
		return scenarioName;
	}

	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}

	public List<SdnNet> getSdnNets() {
		return sdnNets;
	}

	public void setSdnNets(List<SdnNet> sdnNets) {
		this.sdnNets = sdnNets;
	}

	public List<Object> getSfis() {
		return sfis;
	}

	public void setSfis(List<Object> sfis) {
		this.sfis = sfis;
	}

	public List<Object> getSfps() {
		return sfps;
	}

	public void setSfps(List<Object> sfps) {
		this.sfps = sfps;
	}

	public List<Object> getSfs() {
		return sfs;
	}

	public void setSfs(List<Object> sfs) {
		this.sfs = sfs;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public List<VNF> getVnfs() {
		return vnfs;
	}

	public void setVnfs(List<VNF> vnfs) {
		this.vnfs = vnfs;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "DeploymentStatus [classifications=" + classifications + ", createdAt=" + createdAt + ", datacenterId="
				+ datacenterId + ", datacenterTenantId=" + datacenterTenantId + ", description=" + description
				+ ", name=" + name + ", nets=" + nets + ", nsdOsmId=" + nsdOsmId + ", scenarioId=" + scenarioId
				+ ", scenarioName=" + scenarioName + ", sdnNets=" + sdnNets + ", sfis=" + sfis + ", sfps=" + sfps
				+ ", sfs=" + sfs + ", tenantId=" + tenantId + ", uuid=" + uuid + ", vnfs=" + vnfs + ", otherProperties="
				+ otherProperties + "]";
	}
}
