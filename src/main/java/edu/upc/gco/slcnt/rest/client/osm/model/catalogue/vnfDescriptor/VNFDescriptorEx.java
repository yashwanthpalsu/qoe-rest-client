package edu.upc.gco.slcnt.rest.client.osm.model.catalogue.vnfDescriptor;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.rest.client.osm.model.common.AdminInfo;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.vnfDescriptor.VNFDescriptor;

public class VNFDescriptorEx extends VNFDescriptor {

	@JsonProperty("_id")
	private String vnfdId;
	
	@JsonProperty("_admin")
	private AdminInfo adminInfo;

	public String getVnfdId() {
		return vnfdId;
	}

	public void setVnfdId(String vnfdId) {
		this.vnfdId = vnfdId;
	}

	public AdminInfo getAdminInfo() {
		return adminInfo;
	}

	public void setAdminInfo(AdminInfo adminInfo) {
		this.adminInfo = adminInfo;
	}

	@Override
	public String toString() {
		return "VNFDescriptorEx [vnfdId=" + vnfdId + ", adminInfo=" + adminInfo + ", getId()=" + getId()
				+ ", getName()=" + getName() + ", getShortName()=" + getShortName() + ", getLogo()=" + getLogo()
				+ ", getVendor()=" + getVendor() + ", getVersion()=" + getVersion() + ", getDescription()="
				+ getDescription() + ", getVnfConfiguration()=" + getVnfConfiguration() + ", getOperationalStatus()="
				+ getOperationalStatus() + ", getManagementInterface()=" + getManagementInterface()
				+ ", getInternalVld()=" + getInternalVld() + ", getIpProfiles()=" + getIpProfiles()
				+ ", getConnectionPoints()=" + getConnectionPoints() + ", getVduList()=" + getVduList()
				+ ", getVduDependecies()=" + getVduDependecies() + ", getServiceFunctionChain()="
				+ getServiceFunctionChain() + ", getServiceFunctionType()=" + getServiceFunctionType()
				+ ", getHttpEndpoints()=" + getHttpEndpoints() + /*", getScalingGroupDescriptor()="
				+ getScalingGroupDescriptor() + */", getMonitoringParam()=" + getMonitoringParam()
				+ ", getPlacementGroups()=" + getPlacementGroups() + ", any()=" + any() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
}
