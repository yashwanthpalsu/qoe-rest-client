package edu.upc.gco.slcnt.rest.client.openstack.nova.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConfirmResize {

	@JsonProperty("confirmResize")
	private String confirmResize = null;
	
	public ConfirmResize (String confirmResize)
	{
		this.confirmResize = confirmResize;
	}

	public String getConfirmResize() {
		return confirmResize;
	}

	public void setConfirmResize(String confirmResize) {
		this.confirmResize = confirmResize;
	}
}
