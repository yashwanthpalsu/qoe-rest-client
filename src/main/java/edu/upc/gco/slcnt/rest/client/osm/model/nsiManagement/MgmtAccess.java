package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MgmtAccess {

	@JsonProperty("interface_id")
	private String interfaceId;
	
	@JsonProperty("required")
	private String required;		// True or False
	
	@JsonProperty("vdu-id")
	private String vduId;
	
	@JsonProperty("vm_id")
	private String vmId;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();

	public String getInterfaceId() {
		return interfaceId;
	}

	public void setInterfaceId(String interfaceId) {
		this.interfaceId = interfaceId;
	}

	public String getRequired() {
		return required;
	}

	public void setRequired(String required) {
		this.required = required;
	}

	public String getVduId() {
		return vduId;
	}

	public void setVduId(String vduId) {
		this.vduId = vduId;
	}

	public String getVmId() {
		return vmId;
	}

	public void setVmId(String vmId) {
		this.vmId = vmId;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}
