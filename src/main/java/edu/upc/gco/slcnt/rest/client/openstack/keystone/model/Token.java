package edu.upc.gco.slcnt.rest.client.openstack.keystone.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Token {

	@JsonProperty("id")
	private String id = null;

	public Token (String id)
	{
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
