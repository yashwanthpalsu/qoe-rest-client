package edu.upc.gco.slcnt.rest.client.osm2.model.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Credentials {

	@JsonProperty("username")
	private String username;
	
	@JsonProperty("password")
	private String password;
	
	@JsonProperty("project_id")
	private String projectName;
	
	public Credentials (String username, String password, String projectName)
	{
		this.username = username;
		this.password = password;
		this.projectName = projectName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	
}
