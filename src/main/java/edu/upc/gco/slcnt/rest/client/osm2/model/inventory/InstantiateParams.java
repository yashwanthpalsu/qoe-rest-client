package edu.upc.gco.slcnt.rest.client.osm2.model.inventory;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InstantiateParams {

	@JsonProperty("ssh_keys")
	private List<String> sshKeys;	//TODO: Check type
	@JsonProperty("nsDescription")
	private String nsDescription;
	@JsonProperty("nsId")
	private String nsId;
	@JsonProperty("vimAccountId")
	private String vimAccountId;
	@JsonProperty("nsName")
	private String nsName;
	
	public List<String> getSshKeys() {
		return sshKeys;
	}
	public void setSshKeys(List<String> sshKeys) {
		this.sshKeys = sshKeys;
	}
	public String getNsDescription() {
		return nsDescription;
	}
	public void setNsDescription(String nsDescription) {
		this.nsDescription = nsDescription;
	}
	public String getNsId() {
		return nsId;
	}
	public void setNsId(String nsId) {
		this.nsId = nsId;
	}
	public String getVimAccountId() {
		return vimAccountId;
	}
	public void setVimAccountId(String vimAccountId) {
		this.vimAccountId = vimAccountId;
	}
	public String getNsName() {
		return nsName;
	}
	public void setNsName(String nsName) {
		this.nsName = nsName;
	}
}
