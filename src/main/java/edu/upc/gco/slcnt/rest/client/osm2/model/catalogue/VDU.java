package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VDU {

	@JsonProperty("count")
	private int count;
	@JsonProperty("cloud-init-file")
	private String cloudInitFile;
	@JsonProperty("description")
	private String description;
	@JsonProperty("image")
	private String image;
	@JsonProperty("vm-flavor")
	private VMFlavor flavor;
	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("interface")
	private List<InterfaceInfo> ifList;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getCloudInitFile() {
		return cloudInitFile;
	}
	public void setCloudInitFile(String cloudInitFile) {
		this.cloudInitFile = cloudInitFile;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public VMFlavor getFlavor() {
		return flavor;
	}
	public void setFlavor(VMFlavor flavor) {
		this.flavor = flavor;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<InterfaceInfo> getIfList() {
		return ifList;
	}
	public void setIfList(List<InterfaceInfo> ifList) {
		this.ifList = ifList;
	}
}
