package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Net {

	@JsonProperty("created")
	private boolean created;
	
	@JsonProperty("datacenter_id")
	private String datacenterId;
	
	@JsonProperty("datacenter_tenant_id")
	private String datacenterTenantId;
	
	@JsonProperty("error_msg")
	private String errorMsg;
	
	@JsonProperty("ns_net_osm_id")
	private String nsNetOsmId;
	
	@JsonProperty("related")
	private String related;
	
	@JsonProperty("sce_net_id")
	private String sceNetId;
	
	@JsonProperty("sdn_net_id")
	private String sdnNetId;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("uuid")
	private String uuid;
	
	@JsonProperty("vim_info")
	private String vimInfoStr;		// JSON format String
	private VimInfo vimInfo;
	
	@JsonProperty("vim_name")
	private String vimName;
	
	@JsonProperty("vim_net_id")
	private String vimNetId;
	
	@JsonProperty("vnf_net_id")
	private String vnfNetId;
	
	@JsonProperty("vnf_net_osm_id")
	private String vnfNetOsmId;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();

	public boolean isCreated() {
		return created;
	}

	public void setCreated(boolean created) {
		this.created = created;
	}

	public String getDatacenterId() {
		return datacenterId;
	}

	public void setDatacenterId(String datacenterId) {
		this.datacenterId = datacenterId;
	}

	public String getDatacenterTenantId() {
		return datacenterTenantId;
	}

	public void setDatacenterTenantId(String datacenterTenantId) {
		this.datacenterTenantId = datacenterTenantId;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getNsNetOsmId() {
		return nsNetOsmId;
	}

	public void setNsNetOsmId(String nsNetOsmId) {
		this.nsNetOsmId = nsNetOsmId;
	}

	public String getRelated() {
		return related;
	}

	public void setRelated(String related) {
		this.related = related;
	}

	public String getSceNetId() {
		return sceNetId;
	}

	public void setSceNetId(String sceNetId) {
		this.sceNetId = sceNetId;
	}

	public String getSdnNetId() {
		return sdnNetId;
	}

	public void setSdnNetId(String sdnNetId) {
		this.sdnNetId = sdnNetId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getVimInfoStr() {
		return vimInfoStr;
	}

	public void setVimInfoStr(String vimInfoStr) {
		this.vimInfoStr = vimInfoStr;
	}
	
	public void setVimInfo (String vimInfoStr)
	{
		ObjectMapper mapper = new ObjectMapper();
		this.vimInfo = null;
		try {
			this.vimInfo = mapper.readValue(vimInfoStr, VimInfo.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public VimInfo getVimInfo()
	{
		setVimInfo (this.vimInfoStr);
		return vimInfo;
	}

	public String getVimName() {
		return vimName;
	}

	public void setVimName(String vimName) {
		this.vimName = vimName;
	}

	public String getVimNetId() {
		return vimNetId;
	}

	public void setVimNetId(String vimNetId) {
		this.vimNetId = vimNetId;
	}

	public String getVnfNetId() {
		return vnfNetId;
	}

	public void setVnfNetId(String vnfNetId) {
		this.vnfNetId = vnfNetId;
	}

	public String getVnfNetOsmId() {
		return vnfNetOsmId;
	}

	public void setVnfNetOsmId(String vnfNetOsmId) {
		this.vnfNetOsmId = vnfNetOsmId;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}
