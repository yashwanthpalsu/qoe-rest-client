package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NSInstanceId {

	@JsonProperty("id")
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "{\n\t\"id\":\"" + id + "\"\n}";
	}
}
