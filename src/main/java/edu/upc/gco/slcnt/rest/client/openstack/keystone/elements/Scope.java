package edu.upc.gco.slcnt.rest.client.openstack.keystone.elements;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.rest.client.openstack.model.keystone.Project;

public class Scope {

	@JsonProperty("project")
	private Project project = null;
	
	public Scope (Project project)
	{
		this.project = project;
	}
	
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
}
