package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

public class VM {

	@JsonProperty("created_at")
	private String createdAt;
	
	@JsonProperty("error_msg")
	private String errorMsg;
	
	@JsonProperty("interfaces")
	private List<Interface> interfaces;
	
	@JsonProperty("ip_address")
	private String ipAddress;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("related")
	private String related;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("uuid")
	private String uuid;
	
	@JsonProperty("vdu_osm_id")
	private String vduOsmId;
	
	@JsonProperty("vim_info")
	private String vimInfoStr;
	private VimInfo vimInfo;
	
	@JsonProperty("vim_name")
	private String vimName;		// Name of the VM in the VIM
	
	@JsonProperty("vim_vm_id")
	private String vimVmId;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public List<Interface> getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(List<Interface> interfaces) {
		this.interfaces = interfaces;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelated() {
		return related;
	}

	public void setRelated(String related) {
		this.related = related;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getVduOsmId() {
		return vduOsmId;
	}

	public void setVduOsmId(String vduOsmId) {
		this.vduOsmId = vduOsmId;
	}

	public String getVimInfoStr() {
		return vimInfoStr;
	}

	public void setVimInfoStr(String vimInfoStr) {
		this.vimInfoStr = vimInfoStr;
	}
	
	public void setVimInfo (String vimInfoStr)
	{
		ObjectMapper mapper = new ObjectMapper();
		this.vimInfo = null;
		try {
			this.vimInfo = mapper.readValue(vimInfoStr, VimInfo.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public VimInfo getVimInfo()
	{
		setVimInfo (this.vimInfoStr);
		return vimInfo;
	}

	public String getVimName() {
		return vimName;
	}

	public void setVimName(String vimName) {
		this.vimName = vimName;
	}

	public String getVimVmId() {
		return vimVmId;
	}

	public void setVimVmId(String vimVmId) {
		this.vimVmId = vimVmId;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}
