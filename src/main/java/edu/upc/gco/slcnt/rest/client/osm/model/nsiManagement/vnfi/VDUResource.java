package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.vnfi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VDUResource {

	@JsonProperty("status")
	private String status;	// ACTIVE, ...
	@JsonProperty("internal-connection-point")
	private List<String> internalCpList;		// Should be of type ConnectionPoint??
	@JsonProperty("vim-id")
	private String vimInstanceId;				// Id of the VM allocated in the VIM
	@JsonProperty("interfaces")
	private List<Interface> interfaceList;
	@JsonProperty("vdu-id-ref")
	private String vduIdRef;
	@JsonProperty("name")
	private String name;
	@JsonProperty("count-index")
	private int countIndex;
	@JsonProperty("_id")
	private String id;
	@JsonProperty("status-detailed")
	private String statusDetailed;
	@JsonProperty("ip-address")
	private String ipAddress;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getInternalCpList() {
		return internalCpList;
	}

	public void setInternalCpList(List<String> internalCpList) {
		this.internalCpList = internalCpList;
	}

	public String getVimInstanceId() {
		return vimInstanceId;
	}

	public void setVimInstanceId(String vimInstanceId) {
		this.vimInstanceId = vimInstanceId;
	}

	public List<Interface> getInterfaceList() {
		return interfaceList;
	}

	public void setInterfaceList(List<Interface> interfaceList) {
		this.interfaceList = interfaceList;
	}

	public String getVduIdRef() {
		return vduIdRef;
	}

	public void setVduIdRef(String vduIdRef) {
		this.vduIdRef = vduIdRef;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCountIndex() {
		return countIndex;
	}

	public void setCountIndex(int countIndex) {
		this.countIndex = countIndex;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatusDetailed() {
		return statusDetailed;
	}

	public void setStatusDetailed(String statusDetailed) {
		this.statusDetailed = statusDetailed;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}
