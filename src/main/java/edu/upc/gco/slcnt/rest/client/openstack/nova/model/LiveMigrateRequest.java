package edu.upc.gco.slcnt.rest.client.openstack.nova.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LiveMigrateRequest {

	@JsonProperty("os-migrateLive")
	private LiveMigrate migrate = null;
	
	public LiveMigrateRequest (LiveMigrate migrate)
	{
		this.migrate = migrate;
	}

	public LiveMigrate getMigrate() {
		return migrate;
	}

	public void setMigrate(LiveMigrate migrate) {
		this.migrate = migrate;
	}
}
