package edu.upc.gco.slcnt.rest.client.osm2.model.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Role {

	@JsonProperty("id")
	private String id;
	
	@JsonProperty("name")
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
