package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.rest.client.osm2.model.common.VnfDescriptorRefElement;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.nsDescriptor.ConstituentVNFD;

/**
 * Resource Object associated to the deployed NS Instance.
 * 
 * @author fernando
 *
 */
public class RO {

	// TODO: Check the Id values => Are them from OpenStack??
	@JsonProperty("nsd_id")
	private String nsdId;
	@JsonProperty("vnfd")
	private List<ConstituentVNFD> vnfdList;
	@JsonProperty("nsr_id")
	private String nsrId;
	@JsonProperty("nsr_status")
	private String nsrStatus;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public String getNsdId() {
		return nsdId;
	}
	public void setNsdId(String nsdId) {
		this.nsdId = nsdId;
	}
	public List<ConstituentVNFD> getVnfdList() {
		return vnfdList;
	}
	public void setVnfdList(List<ConstituentVNFD> vnfdList) {
		this.vnfdList = vnfdList;
	}
	public String getNsrId() {
		return nsrId;
	}
	public void setNsrId(String nsrId) {
		this.nsrId = nsrId;
	}
	public String getNsrStatus() {
		return nsrStatus;
	}
	public void setNsrStatus(String nsrStatus) {
		this.nsrStatus = nsrStatus;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
	
    @Override
	public String toString() {
		return "RO [nsdId=" + nsdId + ", vnfdList=" + vnfdList + ", nsrId=" + nsrId + ", nsrStatus=" + nsrStatus
				+ ", otherProperties=" + otherProperties + "]";
	}
}
