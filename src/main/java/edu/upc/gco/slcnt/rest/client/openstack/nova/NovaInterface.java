package edu.upc.gco.slcnt.rest.client.openstack.nova;

import org.springframework.http.ResponseEntity;

public interface NovaInterface {

	ResponseEntity<?> getFlavors();
	ResponseEntity<?> getResizeStatus();
	void resize();
	void liveMigrate();
}
