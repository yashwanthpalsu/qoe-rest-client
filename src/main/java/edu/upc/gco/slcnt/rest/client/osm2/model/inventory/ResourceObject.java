package edu.upc.gco.slcnt.rest.client.osm2.model.inventory;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.rest.client.osm2.model.common.VnfDescriptorRefElement;

public class ResourceObject {

	@JsonProperty("nsd_id")
	private String nsdId;
	@JsonProperty("vnfd")
	private List<VnfDescriptorRefElement> vnfdList;
	@JsonProperty("nsr_id")
	private String nsrId;
	@JsonProperty("nsr_status")
	private String nsrStatus;
	
	public String getNsdId() {
		return nsdId;
	}
	public void setNsdId(String nsdId) {
		this.nsdId = nsdId;
	}
	public List<VnfDescriptorRefElement> getVnfdList() {
		return vnfdList;
	}
	public void setVnfdList(List<VnfDescriptorRefElement> vnfdList) {
		this.vnfdList = vnfdList;
	}
	public String getNsrId() {
		return nsrId;
	}
	public void setNsrId(String nsrId) {
		this.nsrId = nsrId;
	}
	public String getNsrStatus() {
		return nsrStatus;
	}
	public void setNsrStatus(String nsrStatus) {
		this.nsrStatus = nsrStatus;
	}
}
