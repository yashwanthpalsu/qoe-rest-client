package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InstantiateParams {

	@JsonProperty("ssh_keys")
	private List<String> sshKeys;	//TODO: Check type
	@JsonProperty("nsDescription")
	private String nsDescription;
	@JsonProperty("nsId")
	private String nsId;
	@JsonProperty("vimAccountId")
	private String vimAccountId;
	@JsonProperty("nsName")
	private String nsName;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public List<String> getSshKeys() {
		return sshKeys;
	}
	public void setSshKeys(List<String> sshKeys) {
		this.sshKeys = sshKeys;
	}
	public String getNsDescription() {
		return nsDescription;
	}
	public void setNsDescription(String nsDescription) {
		this.nsDescription = nsDescription;
	}
	public String getNsId() {
		return nsId;
	}
	public void setNsId(String nsId) {
		this.nsId = nsId;
	}
	public String getVimAccountId() {
		return vimAccountId;
	}
	public void setVimAccountId(String vimAccountId) {
		this.vimAccountId = vimAccountId;
	}
	public String getNsName() {
		return nsName;
	}
	public void setNsName(String nsName) {
		this.nsName = nsName;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
    
	@Override
	public String toString() {
		return "InstantiateParams [sshKeys=" + sshKeys + ", nsDescription=" + nsDescription + ", nsId=" + nsId
				+ ", vimAccountId=" + vimAccountId + ", nsName=" + nsName + ", otherProperties=" + otherProperties
				+ "]";
	}
}
