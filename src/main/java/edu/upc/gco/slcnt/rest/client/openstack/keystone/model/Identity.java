package edu.upc.gco.slcnt.rest.client.openstack.keystone.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Identity {

	@JsonProperty("methods")
	private ArrayList<String> methods = null;
	
	@JsonProperty("password")
	private Password password = null;
	
	@JsonProperty("token")
	private Token token = null;
	
	public Identity (ArrayList<String> methods, Password password, Token token)
	{
		this.methods = methods;
		this.password = password;
		this.token = token;
	}

	public ArrayList<String> getMethods() {
		return methods;
	}

	public void setMethods(ArrayList<String> methods) {
		this.methods = methods;
	}

	public Password getPassword() {
		return password;
	}

	public void setPassword(Password password) {
		this.password = password;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}
}
