package edu.upc.gco.slcnt.rest.client.osm2.model.catalogue;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VnfDescriptorCPRefElement {

	@JsonProperty("member-vnf-index-ref")
	private int vnfIndex;
	@JsonProperty("vnfd-connection-point-ref")
	private String vnfdCPName;
	@JsonProperty("vnfd-id-ref")
	private String vnfdName;
	
	public int getVnfIndex() {
		return vnfIndex;
	}
	public void setVnfIndex(int vnfIndex) {
		this.vnfIndex = vnfIndex;
	}
	public String getVnfdCPName() {
		return vnfdCPName;
	}
	public void setVnfdCPName(String vnfdCPName) {
		this.vnfdCPName = vnfdCPName;
	}
	public String getVnfdName() {
		return vnfdName;
	}
	public void setVnfdName(String vnfdName) {
		this.vnfdName = vnfdName;
	}
}
