package edu.upc.gco.slcnt.rest.client.osm.model.osmManagement;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeployedObject {

	@JsonProperty("RO")
	private String roId;
	@JsonProperty("RO-account")
	private String roAccount;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public String getRoId() {
		return roId;
	}

	public void setRoId(String roId) {
		this.roId = roId;
	}

	public String getRoAccount() {
		return roAccount;
	}

	public void setRoAccount(String roAccount) {
		this.roAccount = roAccount;
	}

	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "DeployedObject [roId=" + roId + ", roAccount=" + roAccount + ", otherProperties=" + otherProperties + "]";
	}
}
