package edu.upc.gco.slcnt.rest.client.openstack.keystone;

import org.springframework.http.ResponseEntity;

public interface KeystoneInterface {

	ResponseEntity<?> getUnscopedToken();
	ResponseEntity<?> createProjectToken();
	ResponseEntity<?> getProjectToken();
	ResponseEntity<?> getUsers();
	ResponseEntity<?> getProjects();
}
