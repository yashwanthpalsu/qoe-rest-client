package edu.upc.gco.slcnt.rest.client.openstack.nova;

import java.io.IOException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.rest.client.Utils;
import edu.upc.gco.slcnt.rest.client.openstack.nova.model.ConfirmResize;
import edu.upc.gco.slcnt.rest.client.openstack.nova.model.LiveMigrate;
import edu.upc.gco.slcnt.rest.client.openstack.nova.model.LiveMigrateRequest;
import edu.upc.gco.slcnt.rest.client.openstack.nova.model.Resize;
import edu.upc.gco.slcnt.rest.client.openstack.nova.model.ResizeRequest;

public class NovaApi {

	private String getResizeStatus (String url, HttpHeaders headers, RestTemplate rt)
	{
		HttpEntity<String> getServerDetails = new HttpEntity<String>(headers);
		ResponseEntity<String> re = rt.exchange(url, HttpMethod.GET, getServerDetails, String.class);
		
		JsonNode json = null;
		String status = null;
		try {
			json = new ObjectMapper().readValue(re.getBody(), JsonNode.class);
			status = json.get("server").get("status").asText();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return status;
	}
	
	public String getFlavorId (String url, String token, String flavorName)
	{
		String flavorId = "";
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Auth-Token", token);
		
		HttpEntity<String> getFlavors = new HttpEntity<String>(headers);
		ResponseEntity<String> re = rt.exchange(url, HttpMethod.GET, getFlavors, String.class);
		
		JsonNode json = null;
		try {
			json = new ObjectMapper().readTree(re.getBody());
			JsonNode flavors = json.get("flavors");
			
			if (flavors.isArray())
			{
				for (int f = 0; f < flavors.size(); f++)
				{
					String name = flavors.get(f).get("name").asText();
					if (name.equals(flavorName))
					{
						flavorId = flavors.get(f).get("id").asText();
						return flavorId;
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return flavorId;
	}
	
	public void resize (String url, String token, String flavorRef)
	{
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("X-Auth-Token", token);
		
		Resize resize = new Resize(flavorRef);
		ResizeRequest resizeReq = new ResizeRequest(resize);
		
		HttpEntity<ResizeRequest> resizeOp = new HttpEntity<ResizeRequest>(resizeReq, headers);
		rt.exchange(url+"/action", HttpMethod.POST, resizeOp, Void.class);
		
		String status = this.getResizeStatus(url, headers, rt);
		
		while (!status.equals("VERIFY_RESIZE"))
		{
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			status = this.getResizeStatus(url, headers, rt);
		}
		
		ConfirmResize confirm = new ConfirmResize("null");
		HttpEntity<ConfirmResize> confirmResizeOp = new HttpEntity<ConfirmResize>(confirm, headers);
		rt.exchange(url+"/action", HttpMethod.POST, confirmResizeOp, Void.class);
	}
	
	public String liveMigrate (String url, String token, String hostId)
	{
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("X-Auth-Token", token);
		
		LiveMigrate migrate = new LiveMigrate(hostId, "false", "false");
		LiveMigrateRequest migrateReq = new LiveMigrateRequest(migrate);
		HttpEntity<LiveMigrateRequest> migrateOp = new HttpEntity<LiveMigrateRequest>(migrateReq, headers);
		ResponseEntity<String>  re = rt.exchange(url+"/action", HttpMethod.POST, migrateOp, String.class);
		
		return re.getBody();
	}
}
