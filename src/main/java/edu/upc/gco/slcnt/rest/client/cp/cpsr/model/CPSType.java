package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum CPSType {
	@JsonProperty("BKH_ADAPTER")
	BKH		("BKH_ADAPTER"),
	@JsonProperty("COR_ADAPTER")
	CORE	("COR_ADAPTER"),
	@JsonProperty("MEC_ADAPTER")
	MEC		("MEC_ADAPTER"),
	@JsonProperty("RAN_ADAPTER")
	RAN		("RAN_ADAPTER"),
	@JsonProperty("WAN_ADAPTER")
	WAN		("WAN_ADAPTER"),
	@JsonProperty("DPP_ADAPTER")
	DPP		("DPP_ADAPTER"),
	@JsonProperty("CNF_CP")
	CNF		("CNF_CP"),
	@JsonProperty("FGE_CP")
	FGE		("FGE_CP"),			// To Remove?
	@JsonProperty("ICP_CP")
	ICP		("ICP_CP"),
	@JsonProperty("ID_CP")
	ID		("ID_CP"),			// Inter-Domain? To Remove? Not a CPF anymore..
	@JsonProperty("PP_CP")
	PP		("PP_CP"),
	@JsonProperty("QOE_CP")
	QOE		("QOE_CP"),
	@JsonProperty("QOS_CP")
	QOS 	("QOS_CP");
	
	protected String value;
	
	private CPSType(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
