package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements;

public class GetPortNumberBody {

	private String node;
	private String ifaceName;
	
	public GetPortNumberBody(String node, String ifaceName) {

		this.node = node;
		this.ifaceName = ifaceName;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getIfaceName() {
		return ifaceName;
	}

	public void setIfaceName(String ifaceName) {
		this.ifaceName = ifaceName;
	}
}