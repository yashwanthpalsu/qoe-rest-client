package edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.upc.gco.slcnt.rest.client.osm.model.catalogue.nsDescriptor.NSDescriptorEx;

/**
 * This is the base object that contains the information of the NS Instance.
 * 
 * @author fernando
 *
 */
public class NSInstance {

	@JsonProperty("name-ref")
	private String nameRef;
	@JsonProperty("vnfd-id")
	private List<String> vnfdIdList;
	@JsonProperty("_admin")
	private AdminObject adminObject;
	@JsonProperty("operational-status")
	private String operationalStatus;					// Values: "running" (if instantiated), "init" (if nsId exists but not instantiated yet), failed, terminating, terminated
	@JsonProperty("orchestration-progress")
	private Object orchProgress;						// TODO: Check type
	@JsonProperty("instantiate_params")
	private InstantiateParams instantiateParams;
	@JsonProperty("short-name")
	private String shortName;
	@JsonProperty("detailed-status")
	private String detailedStatus;						// Values: "done" (if instantiated), "scheduled" (if nsId exists but not instantiated yet)
	@JsonProperty("ssh-authorized-key")
	private List<String> sshAuthKeys;					// TODO: Check type
	@JsonProperty("operational-events")
	private List<String> operationalEvents;				// TODO: Check type
	@JsonProperty("nsd-ref")
	private String nsdRef;
	@JsonProperty("additionalParamsForNs")
	private Object additionalParams;					// TODO: Check type
	@JsonProperty("id")
	private String id;
	@JsonProperty("admin-status")
	private String adminStatus;							// Example value: "ENABLED"
	@JsonProperty("ns-instance-config-ref")
	private String nsInstanceConfigRef;					// == id
	@JsonProperty("datacenter")
	private String datacenterId;						// VIM Id at OSM
	@JsonProperty("nsd")
	private NSDescriptorEx nsd;
	@JsonProperty("nsd-name-ref")
	private String nsdNameRef;							// == NSD name
	@JsonProperty("constituent-vnfr-ref")
	private List<String> vnfrList;						// List of VNF Instance Ids
	@JsonProperty("nsd-id")
	private String nsdId;								// == nsd -> _id
	@JsonProperty("config-status")
	private String configStatus;						// Values: "configured" (if instantiated), "init" (if nsId exists but not instantiated yet)
	@JsonProperty("description")
	private String description;
	@JsonProperty("name")
	private String name;
	@JsonProperty("resource-orchestrator")
	private String resourceOrch;
	@JsonProperty("vld")
	private List<VLD> vldList;
	@JsonProperty("create-time")
	private float createTime;
	@JsonProperty("_id")
	private String nsiId;								// == id
	@JsonProperty("deploymentStatus")
	private DeploymentStatus deploymentStatus;
	
	@Transient
    private Map<String, Object> otherProperties = new HashMap<String, Object>();
	
	public String getNameRef() {
		return nameRef;
	}
	public void setNameRef(String nameRef) {
		this.nameRef = nameRef;
	}
	public List<String> getVnfdIdList() {
		return vnfdIdList;
	}
	public void setVnfdIdList(List<String> vnfdIdList) {
		this.vnfdIdList = vnfdIdList;
	}
	public AdminObject getAdminObject() {
		return adminObject;
	}
	public void setAdminObject(AdminObject adminObject) {
		this.adminObject = adminObject;
	}
	public String getOperationalStatus() {
		return operationalStatus;
	}
	public void setOperationalStatus(String operationalStatus) {
		this.operationalStatus = operationalStatus;
	}
	public Object getOrchProgress() {
		return orchProgress;
	}
	public void setOrchProgress(Object orchProgress) {
		this.orchProgress = orchProgress;
	}
	public InstantiateParams getInstantiateParams() {
		return instantiateParams;
	}
	public void setInstantiateParams(InstantiateParams instantiateParams) {
		this.instantiateParams = instantiateParams;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getDetailedStatus() {
		return detailedStatus;
	}
	public void setDetailedStatus(String detailedStatus) {
		this.detailedStatus = detailedStatus;
	}
	public List<String> getSshAuthKeys() {
		return sshAuthKeys;
	}
	public void setSshAuthKeys(List<String> sshAuthKeys) {
		this.sshAuthKeys = sshAuthKeys;
	}
	public List<String> getOperationalEvents() {
		return operationalEvents;
	}
	public void setOperationalEvents(List<String> operationalEvents) {
		this.operationalEvents = operationalEvents;
	}
	public String getNsdRef() {
		return nsdRef;
	}
	public void setNsdRef(String nsdRef) {
		this.nsdRef = nsdRef;
	}
	public Object getAdditionalParams() {
		return additionalParams;
	}
	public void setAdditionalParams(Object additionalParams) {
		this.additionalParams = additionalParams;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAdminStatus() {
		return adminStatus;
	}
	public void setAdminStatus(String adminStatus) {
		this.adminStatus = adminStatus;
	}
	public String getNsInstanceConfigRef() {
		return nsInstanceConfigRef;
	}
	public void setNsInstanceConfigRef(String nsInstanceConfigRef) {
		this.nsInstanceConfigRef = nsInstanceConfigRef;
	}
	public String getDatacenterId() {
		return datacenterId;
	}
	public void setDatacenterId(String datacenterId) {
		this.datacenterId = datacenterId;
	}
	public NSDescriptorEx getNsd() {
		return nsd;
	}
	public void setNsd(NSDescriptorEx nsd) {
		this.nsd = nsd;
	}
	public String getNsdNameRef() {
		return nsdNameRef;
	}
	public void setNsdNameRef(String nsdNameRef) {
		this.nsdNameRef = nsdNameRef;
	}
	public List<String> getVnfrList() {
		return vnfrList;
	}
	public void setVnfrList(List<String> vnfrList) {
		this.vnfrList = vnfrList;
	}
	public String getNsdId() {
		return nsdId;
	}
	public void setNsdId(String nsdId) {
		this.nsdId = nsdId;
	}
	public String getConfigStatus() {
		return configStatus;
	}
	public void setConfigStatus(String configStatus) {
		this.configStatus = configStatus;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getResourceOrch() {
		return resourceOrch;
	}
	public void setResourceOrch(String resourceOrch) {
		this.resourceOrch = resourceOrch;
	}
	public List<VLD> getVldList() {
		return vldList;
	}
	public void setVldList(List<VLD> vldList) {
		this.vldList = vldList;
	}
	public float getCreateTime() {
		return createTime;
	}
	public void setCreateTime(float createTime) {
		this.createTime = createTime;
	}
	public String getNsiId() {
		return nsiId;
	}
	public void setNsiId(String nsiId) {
		this.nsiId = nsiId;
	}
	
	public DeploymentStatus getDeploymentStatus() {
		return deploymentStatus;
	}
	public void setDeploymentStatus(DeploymentStatus deploymentStatus) {
		this.deploymentStatus = deploymentStatus;
	}
	
	@JsonAnyGetter
    public Map<String, Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
	
    @Override
	public String toString() {
		return "NSInstance [nameRef=" + nameRef + ", vnfdIdList=" + vnfdIdList + ", adminObject=" + adminObject
				+ ", operationalStatus=" + operationalStatus + ", orchProgress=" + orchProgress + ", instantiateParams="
				+ instantiateParams + ", shortName=" + shortName + ", detailedStatus=" + detailedStatus
				+ ", sshAuthKeys=" + sshAuthKeys + ", operationalEvents=" + operationalEvents + ", nsdRef=" + nsdRef
				+ ", additionalParams=" + additionalParams + ", id=" + id + ", adminStatus=" + adminStatus
				+ ", nsInstanceConfigRef=" + nsInstanceConfigRef + ", datacenterId=" + datacenterId + ", nsd=" + nsd
				+ ", nsdNameRef=" + nsdNameRef + ", vnfrList=" + vnfrList + ", nsdId=" + nsdId + ", configStatus="
				+ configStatus + ", description=" + description + ", name=" + name + ", resourceOrch=" + resourceOrch
				+ ", vldList=" + vldList + ", createTime=" + createTime + ", nsiId=" + nsiId + ", deploymentStatus=" + deploymentStatus + ", otherProperties="
				+ otherProperties + "]";
	}
}
