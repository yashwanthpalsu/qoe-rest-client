package edu.upc.gco.slcnt.rest.client.openstack.nova.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Resize {

	@JsonProperty("flavorRef")
	private String flavorRef = null;
	
	public Resize (String flavorRef)
	{
		this.flavorRef = flavorRef;
	}

	public String getFlavorRef() {
		return flavorRef;
	}

	public void setFlavorRef(String flavorRef) {
		this.flavorRef = flavorRef;
	}
}
