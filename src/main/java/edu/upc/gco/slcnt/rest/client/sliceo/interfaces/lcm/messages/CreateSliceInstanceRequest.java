package edu.upc.gco.slcnt.rest.client.sliceo.interfaces.lcm.messages;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class CreateSliceInstanceRequest implements InterfaceMessage {

	private String sliceTemplateId;
	
	private String tenantId;
	
	public CreateSliceInstanceRequest ()
	{
		
	}
	
	public CreateSliceInstanceRequest (String sliceTemplateId, String tenantId)
	{
		this.sliceTemplateId = sliceTemplateId;
		this.tenantId = tenantId;
	}

	public String getSliceTemplateId() {
		return sliceTemplateId;
	}

	public String getTenantId() {
		return tenantId;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		
		if (sliceTemplateId == null) throw new MalformattedElementException("Slice Instance Creation request without Slice Template ID");
		if (tenantId == null) throw new MalformattedElementException("Slice Instance Creation request without DSP Tenant ID");
	}
}
