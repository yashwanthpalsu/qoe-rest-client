package edu.upc.gco.slcnt.rest.client.sliceo.interfaces.lcm.messages;

import java.util.Map;

import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class UpdateSliceRequest implements InterfaceMessage {

	private String sliceId;
	private String operationName;
	private Map<String,String> parameters;
	
	public UpdateSliceRequest ()
	{
		
	}
	
	public UpdateSliceRequest (String sliceId, String operationName, Map<String,String> parameters)
	{
		this.sliceId = sliceId;
		this.operationName = operationName;
		this.parameters = parameters;
	}
	
	public String getSliceId() {
		return sliceId;
	}

	public String getOperationName() {
		return operationName;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setSliceId(String sliceId) {
		this.sliceId = sliceId;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (sliceId == null) throw new MalformattedElementException("Update Slice request without Slice instance ID");
		if (operationName == null) throw new MalformattedElementException("Update Slice request without Operation Name");
	}
}
