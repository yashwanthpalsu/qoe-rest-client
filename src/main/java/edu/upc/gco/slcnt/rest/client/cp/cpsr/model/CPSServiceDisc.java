package edu.upc.gco.slcnt.rest.client.cp.cpsr.model;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CPSServiceDisc {
	
	@JsonProperty("serviceInstanceId")
	private UUID instanceId;
	@JsonProperty("serviceName")
	private String name;
	@JsonProperty("version")
	private String version;
	@JsonProperty("schema")
	private String schema;
	@JsonProperty("fqdn")
	private String fqdn;
	@JsonProperty("ipEndPoints")
	private List<IpEndPoint> endPoints;
	@JsonProperty("apiPrefix")
	private String apiPrefix;
	@JsonProperty("defaultNotificationSubscriptions")
	private List<DefaultNotificationSubscription> subscriptions;
	@JsonProperty("load")
	private int load;
	@JsonProperty("capacity")
	private int capacity;
	@JsonProperty("priority")
	private int priority;

	public UUID getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(UUID instanceId) {
		this.instanceId = instanceId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	public String getFqdn() {
		return fqdn;
	}
	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}
	public List<IpEndPoint> getEndPoints() {
		return endPoints;
	}
	public void setEndPoints(List<IpEndPoint> endPoints) {
		this.endPoints = endPoints;
	}
	public String getApiPrefix() {
		return apiPrefix;
	}
	public void setApiPrefix(String apiPrefix) {
		this.apiPrefix = apiPrefix;
	}
	public List<DefaultNotificationSubscription> getSubscriptions() {
		return subscriptions;
	}
	public void setSubscriptions(List<DefaultNotificationSubscription> subscriptions) {
		this.subscriptions = subscriptions;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public int getLoad() {
		return load;
	}
	public void setLoad(int load) {
		this.load = load;
	}	
}
