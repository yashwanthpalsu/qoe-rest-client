package edu.upc.gco.slcnt.rest.client.osdncontroller.interfaces.sbi.elements;

import java.util.HashMap;

public class Topology {
	
	private HashMap<String,Node> nodes;
	private HashMap<String,Link> links;

	public Topology() {
		
		this.nodes = new HashMap<String,Node>();
		this.links = new HashMap<String,Link>();
	}

	public HashMap<String, Node> getNodes() {
		return nodes;
	}

	public void setNodes(HashMap<String, Node> nodes) {
		this.nodes = nodes;
	}

	public HashMap<String, Link> getLinks() {
		return links;
	}

	public void setLinks(HashMap<String, Link> links) {
		this.links = links;
	}
}