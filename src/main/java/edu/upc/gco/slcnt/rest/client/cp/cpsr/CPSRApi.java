package edu.upc.gco.slcnt.rest.client.cp.cpsr;

import java.io.IOException;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.rest.client.Utils;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSProfile;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSRegistrationData;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSStatus;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSType;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.PatchDocument;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.PatchItem;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.ProblemDetails;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.SearchResult;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.PatchItem.OpEnum;

public class CPSRApi {
	
	private Log logger = LogFactory.getLog(CPSRApi.class);
	
	public ResponseEntity<String> registerCPSInstance (String baseUrl, CPSProfile cpsProfile)
	{
		String url = baseUrl + "/slicenet/ctrlplane/cpsr_cps/v1/cps-instances/" + cpsProfile.getInstanceId();
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	
		HttpEntity<CPSProfile> request = new HttpEntity<CPSProfile>(cpsProfile, headers);
		logger.info("PUT: " + url);
		logger.info("JSON: " + request.toString());
		ResponseEntity<String> response = rt.exchange(url, HttpMethod.PUT, request, String.class);
		logger.info("Response: " + response.getStatusCodeValue());
		
		return response;
	}
	
	public CPSProfile searchCPSInstances (String baseUrl, CPSType cpsType, UUID sliceId)
	{
		//SearchCPSProfile
		String searchUrl = baseUrl + "/slicenet/ctrlplane/cpsr_disc/v1/cps-instances?cpsType=" + cpsType.getValue() + "&slicenetId=" + sliceId;
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
    	
		//SearchResult sres = rt.getForObject(searchUrl, SearchResult.class);
    	ResponseEntity<SearchResult> response = rt.getForEntity(searchUrl, SearchResult.class);
		return null;
	}
	
	public ResponseEntity<String> getCPSInstances (String baseUrl, String sliceId, String cpsType)
	{
		String url = baseUrl + "/slicenet/ctrlplane/cpsr_cps/v1/cps-instances?cpsType=" + cpsType + "&slicenetId=" + sliceId;
		logger.info("GET " + url);
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		
		ResponseEntity<String> response = rt.getForEntity(url, String.class);
		
		return response;
		/*String cpfURI = "";
		try
		{
			cpfURI = rt.getForObject(url, String.class);
		}
		catch (RestClientException rce)
		{
			logger.error(rce.getMessage());
			return url;
		}
		
		ObjectMapper mapper = new ObjectMapper();
		String strURI = "-";
		try {
			//JsonNode root = mapper.readTree(cpfURI);
			//JsonNode uri = root.get("eNB_config");
			//if (uri.isArray())
			//	strURI = "" + uri.get(0).get("eNBId").asInt();
			JsonNode root = mapper.readTree(cpfURI);
			JsonNode cpss = root.get("cpss");
			if (cpss.isArray())
				strURI = cpss.get(0).get("uri").asText();
		} catch (IOException e) {
			logger.error("Error processing the request: " + e.getMessage());
		}
		return strURI;
		//return url;*/
	}
	
	public String requestFunction_old (String baseUrl, String sliceId, String cpsType)
	{
		String url = baseUrl + "/ctrlplane/cpsr/v1/cps-instances?CPS-Type=" + cpsType + "&sliceId=" + sliceId;
		//String url = "http://193.55.112.80:9999/stats";
		logger.info("GET " + url);
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
		String cpfURI = "";
		try
		{
			cpfURI = rt.getForObject(url, String.class);
		}
		catch (RestClientException rce)
		{
			logger.error(rce.getMessage());
			return url;
		}
		
		ObjectMapper mapper = new ObjectMapper();
		String strURI = "-";
		try {
			//JsonNode root = mapper.readTree(cpfURI);
			//JsonNode uri = root.get("eNB_config");
			//if (uri.isArray())
			//	strURI = "" + uri.get(0).get("eNBId").asInt();
			JsonNode root = mapper.readTree(cpfURI);
			JsonNode cpss = root.get("cpss");
			if (cpss.isArray())
				strURI = cpss.get(0).get("uri").asText();
		} catch (IOException e) {
			logger.error("Error processing the request: " + e.getMessage());
		}
		return strURI;
		//return url;
	}
	
	public HttpStatus sendHeartBeat (String baseUrl, UUID cpsInstanceId)
	{
		PatchDocument patchDocument = new PatchDocument();
		PatchItem heartBeat = new PatchItem();
		heartBeat.setOp(OpEnum.replace);
		heartBeat.setPath("/cpsStatus");
		heartBeat.setValue(CPSStatus.REGISTERED);
		patchDocument.add(heartBeat);
		
		String url = baseUrl + "/slicenet/ctrlplane/cpsr_cps/v1/cps-instances/" + cpsInstanceId;
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
    	HttpHeaders headers = new HttpHeaders();
    	//headers.setContentType(MediaType.APPLICATION_JSON);
    	headers.set("Content-Type", "application/json-patch+json");
    	
    	HttpEntity<PatchDocument> request = new HttpEntity<PatchDocument>(patchDocument, headers);
		logger.debug("PATCH: " + url);
		
		//ResponseEntity<String> response = rt.exchange(url, HttpMethod.PATCH, request, String.class);
		ResponseEntity<Void> response = null;
		
		try
		{
			response = rt.exchange(url, HttpMethod.PATCH, request, Void.class);
		}
		catch (HttpClientErrorException e)
		{
			logger.error("PATCH Error: " + e.getMessage());
		}
		
		if (response == null)
			return null;
		
		return response.getStatusCode();
	}
	
	public void sendPatch ()
	{
		//TODO: Implement Patch command to Update Services
		logger.info("Send Patch to the CPSR");
	}
	
	public void deregisterCPSInstance (String baseUrl, UUID cpsInstanceId)
	{
		String url = baseUrl + "/slicenet/ctrlplane/cpsr_cps/v1/cps-instances/" + cpsInstanceId;
		
		RestTemplate rt = new RestTemplate(Utils.getClientHttpRequestFactory());
    	
		logger.info("DELETE: " + url);
		rt.delete(url);
	}
}
