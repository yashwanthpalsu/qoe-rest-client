package edu.upc.gco.slcnt.websocket.client.pf.spring;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

public class PFStompSessionHandler extends StompSessionHandlerAdapter {

	private Log logger = LogFactory.getLog(PFStompSessionHandler.class);
	
	@Override
	public void afterConnected (StompSession session, StompHeaders connectedHeaders)
	{
		logger.info("New session established: " + session.getSessionId());
		session.subscribe("/pdp/notifications", this);
	}
	
	@Override
	public void handleException (StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception)
	{
		logger.error("Error: " + exception.getMessage());
	}
}
