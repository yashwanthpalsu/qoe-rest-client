package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Sensor {

	@JsonProperty("configuration")
	private Configuration configuration;
	
	@JsonProperty("output")
	private Output output;
	
	@JsonProperty("OID")
	private String oid;
	
	@JsonProperty("description")
	private String description;

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public Output getOutput() {
		return output;
	}

	public void setOutput(Output output) {
		this.output = output;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "[configuration=" + configuration + ", output=" + output + ", oid=" + oid + ", description="
				+ description + "]";
	}
}
