package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Controller {

	@JsonProperty("actuator")
	private List<ActionOption> actuators;

	public List<ActionOption> getActuators() {
		return actuators;
	}

	public void setActuators(List<ActionOption> actuators) {
		this.actuators = actuators;
	}

	@Override
	public String toString() {
		return "Controller [actuators=" + actuators + "]";
	}
}
