package edu.upc.gco.slcnt.pf.client.websocket.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Matches {

	@JsonProperty("ONAPName")
	private String onapName = null;
	
	@JsonProperty("ConfigName")
	private String configName = null;
	
	@JsonProperty("service")
	private String service = null;
	
	@JsonProperty("guard")
	private Boolean guard = null;
	
	@JsonProperty("location")
	private String location = null;
	
	@JsonProperty("TTLDate")
	private String ttlDate = null;
	
	@JsonProperty("uuid")
	private String uuid = null;
	
	@JsonProperty("RiskLevel")
	private Integer riskLevel = null;
	
	@JsonProperty("RiskType")
	private String riskType = null;

	public String getOnapName() {
		return onapName;
	}

	public void setOnapName(String onapName) {
		this.onapName = onapName;
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public Boolean getGuard() {
		return guard;
	}

	public void setGuard(Boolean guard) {
		this.guard = guard;
	}

	public String getTtlDate() {
		return ttlDate;
	}

	public void setTtlDate(String ttlDate) {
		this.ttlDate = ttlDate;
	}

	public String getUuid() {
		return uuid;
	}

	public Integer getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(Integer riskLevel) {
		this.riskLevel = riskLevel;
	}

	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	
	@Override
	public String toString()
	{
		String str = "";
		
		str += " { ONAPName=" + this.onapName + ", ConfigName=" + this.configName + " } ";
		
		return str;
	}
}
