package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BootstrapItem {

	// Choice
	@JsonProperty("sensor")
	private Sensor sensor;
	
	@JsonProperty("actuator")
	private Actuator actuator;
	// End Choice
	
	//TODO: Elaborate metadata
	@JsonProperty("metadata")
	private String metadata;

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public Actuator getActuator() {
		return actuator;
	}

	public void setActuator(Actuator actuator) {
		this.actuator = actuator;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	@Override
	public String toString() {
		return "[sensor=" + sensor + ", actuator=" + actuator + ", metadata=" + metadata + "]";
	}
}
