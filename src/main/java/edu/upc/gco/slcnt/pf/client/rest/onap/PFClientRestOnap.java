package edu.upc.gco.slcnt.pf.client.rest.onap;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.pf.client.rest.PFClientRest;
import edu.upc.gco.slcnt.pf.client.rest.onap.model.Policy;
import edu.upc.gco.slcnt.pf.client.rest.onap.model.PolicyRequest;
import edu.upc.gco.slcnt.rest.client.Utils;

public class PFClientRestOnap extends PFClientRest {

	private HttpComponentsClientHttpRequestFactory requestFactory;
	
	public PFClientRestOnap(String sliceId, String ipAddress, String port, String protocol) {
		super(sliceId, ipAddress, port, protocol);
		
		requestFactory = Utils.getSslDisabledClientHttpRequestFactory();
		restTemplate = new RestTemplate(requestFactory);
	}

	public Policy getPolicy (String policyName, String policyVersion)
	{
		String url = protocol + "://" + ipAddress + ":" + port + pfRest + "/getConfig";
		logger.info("URL= " + url);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> mediaTypes = new ArrayList<MediaType>();
		mediaTypes.add(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypes);
		headers.set("ClientAuth","cHl0aG9uOnRlc3Q=");
		headers.set("Authorization","Basic dGVzdHBkcDphbHBoYTEyMw==");
		headers.set("Environment","TEST");
		
		PolicyRequest policyRequest = new PolicyRequest();
		policyRequest.setPolicyName(policyName);
		policyRequest.setRequestID(UUID.randomUUID());
		HttpEntity<PolicyRequest> request = new HttpEntity<PolicyRequest>(policyRequest,headers);
		ResponseEntity<List<Policy>> response = restTemplate.exchange(url, HttpMethod.POST, request,new ParameterizedTypeReference<List<Policy>>() {});
		logger.info("Status = " + response.getStatusCodeValue());
		
		// There should be just one element in the list!
		/*List<TAL> sliceTalPolicies = new ArrayList<TAL>();
		for (Policy p : response.getBody())
		{
			if (p.getMatchingConditions() != null && p.getMatchingConditions().getUuid() != null && p.getMatchingConditions().getUuid().equals(sliceId))
			{
				logger.info("Adding Policy: " + p.getPolicyName());
				//slicePolicies.add(p);
				
				String strPolicy = p.getConfig();
				String jsonPolicy = strPolicy;
				logger.info("strPolicy = " + jsonPolicy);
				ObjectMapper mapper = new ObjectMapper();
				Config config = null;
				try {
					config = mapper.readValue(jsonPolicy, Config.class);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (config != null)
				{
					logger.info("TAL = " + config.getContent().toString());
					sliceTalPolicies.add(config.getContent());
				}
			}
		}
		
		return sliceTalPolicies.get(0);*/
		
		// There should be just one element in the list!
		List<Policy> slicePolicies = new ArrayList<Policy>();
    	for (Policy p : response.getBody())
		{
    		if (p.getMatchingConditions() != null && p.getMatchingConditions().getUuid() != null && p.getMatchingConditions().getUuid().equals(sliceId))
			{
    			logger.info("Adding Policy: " + p.getPolicyName());
				slicePolicies.add(p);
			}
		}
		
    	return slicePolicies.get(0);
	}
	
	public List<Policy> getPolicies ()
	{
		String url = protocol + "://" + ipAddress + ":" + port + pfRest + "/getConfig";
		logger.info("URL= " + url);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> mediaTypes = new ArrayList<MediaType>();
		mediaTypes.add(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypes);
		headers.set("ClientAuth","cHl0aG9uOnRlc3Q=");
		headers.set("Authorization","Basic dGVzdHBkcDphbHBoYTEyMw==");
		headers.set("Environment","TEST");
		
		PolicyRequest policyRequest = new PolicyRequest();
		policyRequest.setPolicyName(".*");
		policyRequest.setRequestID(UUID.randomUUID());
		HttpEntity<PolicyRequest> request = new HttpEntity<PolicyRequest>(policyRequest,headers);
		ResponseEntity<List<Policy>> response = restTemplate.exchange(url, HttpMethod.POST, request,new ParameterizedTypeReference<List<Policy>>() {});
		logger.info("Status = " + response.getStatusCodeValue());
		
		
		/*List<TAL> sliceTalPolicies = new ArrayList<TAL>();
		for (Policy p : response.getBody())
		{
			if (p.getMatchingConditions() != null && p.getMatchingConditions().getUuid() != null && p.getMatchingConditions().getUuid().equals(sliceId))
			{
				logger.info("Adding Policy: " + p.getPolicyName());
				//slicePolicies.add(p);
				
				String strPolicy = p.getConfig();
				String jsonPolicy = strPolicy;
				logger.info("strPolicy = " + jsonPolicy);
				ObjectMapper mapper = new ObjectMapper();
				Config config = null;
				try {
					config = mapper.readValue(jsonPolicy, Config.class);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (config != null)
				{
					logger.info("TAL = " + config.getContent().toString());
					sliceTalPolicies.add(config.getContent());
				}
			}
		}
		return sliceTalPolicies;
		*/
		
		List<Policy> slicePolicies = new ArrayList<Policy>();
    	for (Policy p : response.getBody())
		{
    		if (p.getMatchingConditions() != null && p.getMatchingConditions().getUuid() != null && p.getMatchingConditions().getUuid().equals(sliceId))
			{
    			logger.info("Adding Policy: " + p.getPolicyName());
				slicePolicies.add(p);
			}
		}
		
    	return slicePolicies;
	}
}
