package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Bootstrap {

	@JsonProperty("bootstrapItem")
	private List<BootstrapItem> bootstrapItem;
	
	@JsonProperty("OID")
	private String OID;

	public List<BootstrapItem> getBootstrapItem() {
		return bootstrapItem;
	}

	public void setBootstrapItem(List<BootstrapItem> bootstrapItem) {
		this.bootstrapItem = bootstrapItem;
	}

	public String getOID() {
		return OID;
	}

	public void setOID(String oID) {
		OID = oID;
	}
	
	@Override
	public String toString() {
		return "[bootstrapItem=" + bootstrapItem + ", OID=" + OID + "]";
	}
}
