package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Reaction {

	@JsonProperty("diagnosis")
	private List<Diagnosis> diagnosis;
	
	@JsonProperty("tactic")
	private List<Tactic> tactic;

	public List<Diagnosis> getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(List<Diagnosis> diagnosis) {
		this.diagnosis = diagnosis;
	}

	public List<Tactic> getTactic() {
		return tactic;
	}

	public void setTactic(List<Tactic> tactic) {
		this.tactic = tactic;
	}

	@Override
	public String toString() {
		return "[diagnosis=" + diagnosis + ", tactic=" + tactic + "]";
	}
}
