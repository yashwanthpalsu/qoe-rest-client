package edu.upc.gco.slcnt.pf.client.websocket.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Notification {

	@JsonProperty("removedPolicies")
	private List<Policy> removedPolicies = null;
	
	@JsonProperty("loadedPolicies")
	private List<Policy> loadedPolicies = null;
	
	@JsonProperty("notificationType")
	private String type = null;

	public List<Policy> getRemovedPolicies() {
		return removedPolicies;
	}

	public void setRemovedPolicies(List<Policy> removedPolicies) {
		this.removedPolicies = removedPolicies;
	}

	public List<Policy> getLoadedPolicies() {
		return loadedPolicies;
	}

	public void setLoadedPolicies(List<Policy> loadedPolicies) {
		this.loadedPolicies = loadedPolicies;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString()
	{
		String str = "";
		
		str += " { removedPolicies = ";
		for (Policy p : this.removedPolicies)
		{
			str += p.toString();
		}
		str += " }, ";
		
		str += " { loadedPolicies = ";
		for (Policy p : this.loadedPolicies)
		{
			str += p.toString();
		}
		str += "}, notificationType = " + this.type + " }";
		
		return str;
	}
}
