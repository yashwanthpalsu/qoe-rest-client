package edu.upc.gco.slcnt.pf.client.websocket;

import edu.upc.gco.slcnt.pf.client.websocket.model.Notification;

public interface NotificationCB {

	public void notificationUpdate (Notification notification);
}
