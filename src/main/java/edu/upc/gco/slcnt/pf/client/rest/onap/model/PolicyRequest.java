package edu.upc.gco.slcnt.pf.client.rest.onap.model;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PolicyRequest {

	@JsonProperty("policyName")
	private String policyName = null;
	
	@JsonProperty("requestID")
	private UUID requestID = null;

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public UUID getRequestID() {
		return requestID;
	}

	public void setRequestID(UUID requestID) {
		this.requestID = requestID;
	}
	
	@Override
	public String toString()
	{
		String str = "";
		
		str += " { policyName=" + this.policyName + " requestID=" + this.requestID + " } ";
		
		return str;
	}
}
