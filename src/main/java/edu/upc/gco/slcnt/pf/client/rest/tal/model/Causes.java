package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Causes {

	@JsonProperty("cause")
	private List<Cause> causes;

	public List<Cause> getCauses() {
		return causes;
	}

	public void setCauses(List<Cause> causes) {
		this.causes = causes;
	}

	@Override
	public String toString() {
		return "[causes=" + causes + "]";
	}
}
