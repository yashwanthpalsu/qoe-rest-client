package edu.upc.gco.slcnt.pf.client;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.upc.gco.slcnt.pf.client.rest.PFClientRest;
import edu.upc.gco.slcnt.pf.client.rest.onap.PFClientRestOnap;
import edu.upc.gco.slcnt.pf.client.rest.onap.model.Policy;
import edu.upc.gco.slcnt.pf.client.websocket.NotificationListener;
import edu.upc.gco.slcnt.pf.client.websocket.PFClientWS;

public class PFApi {

	private Log logger = LogFactory.getLog(PFApi.class);
	private String sliceId, ipAddress, port, restProtocol, wsProtocol = "wss";
	private PFClientRest restClient;
	private PFClientWS wsClient;
	private NotificationListener notificationListener;
	
	private final String notificationsUrl = "/pdp/notifications";
	
	public PFApi (String sliceId, String protocol, String ipAddress, String port)
	{
		this.sliceId = sliceId;
		this.ipAddress = ipAddress;
		this.port = port;
		this.restProtocol = protocol;
		
		this.restClient = new PFClientRestOnap(this.sliceId, ipAddress, port, restProtocol);
	}
	
	// Trust all certificates
	private TrustManager[] createTrustManager ()
	{
		TrustManager[] trustAllCerts = new TrustManager[]
		{
			new X509TrustManager()
			{
				public X509Certificate[] getAcceptedIssuers() { 
					return new X509Certificate[0];
				} 
				public void checkClientTrusted( 
					X509Certificate[] certs, String authType) {
				} 
				public void checkServerTrusted( 
					X509Certificate[] certs, String authType) {
				}
			}
		};
		
		return trustAllCerts;
	}
	
	// API 
	
	public List<Policy> getPolicies ()
	{
		return restClient.getPolicies();
	}
	
	public Policy getPolicy (String policyName, String policyVersion)
	{
		return restClient.getPolicy(policyName, policyVersion);
	}
	
	public void connect (NotificationListener notificationListener)
	{
		this.notificationListener = notificationListener;
		URI endpointUri = null;
		try {
			endpointUri = new URI(wsProtocol + "://" + ipAddress + ":" + port + notificationsUrl);
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		this.wsClient = new PFClientWS (endpointUri, this.notificationListener);
		
		SSLContext context = null;
		try {
			context = SSLContext.getInstance("TLSv1.2");
			//context.init(null, null, null);
			context.init(null, this.createTrustManager(), new SecureRandom());
		}
		catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (KeyManagementException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		SSLSocketFactory factory = context.getSocketFactory();
		
		this.wsClient.setSocketFactory(factory);
		try {
			logger.info("Connecting...");
			boolean connected = this.wsClient.connectBlocking();
			logger.info("Connected... " + connected);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void disconnect ()
	{
		logger.debug("Disconnecting...");
		this.wsClient.close();
	}

	public PFClientWS getWsClient() {
		return wsClient;
	}

	public void setWsClient(PFClientWS wsClient) {
		this.wsClient = wsClient;
	}
}
