package edu.upc.gco.slcnt.pf.client.rest.onap.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MatchingConditions {

	@JsonProperty("ECOMName")
	private String ecomName = null;
	
	@JsonProperty("ONAPName")
	private String onapName = null;
	
	@JsonProperty("ConfigName")
	private String configName = null;
	
	@JsonProperty("service")
	private String service = null;
	
	@JsonProperty("uuid")
	private String uuid = null;
	
	@JsonProperty("Location")
	private String location = null;
	
	public String getService() {
		return service;
	}

	public String getUuid() {
		return uuid;
	}

	@Override
	public String toString()
	{
		String str = "";
		
		str += " { ECOMName=" + this.ecomName + ", ONAPName=" + this.onapName + ", ConfigName=" + this.configName + ", service=" + this.service + ", uuid=" + this.uuid+ ", Location=" + this.location + " } ";
		
		return str;
	}
}
