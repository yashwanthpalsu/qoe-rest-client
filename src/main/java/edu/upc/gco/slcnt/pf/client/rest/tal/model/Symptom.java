package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Symptom {

	@JsonProperty("analysis")
	private Analysis analysis;
	
	@JsonProperty("description")
	private String description;

	public Analysis getAnalysis() {
		return analysis;
	}

	public void setAnalysis(Analysis analysis) {
		this.analysis = analysis;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "[analysis=" + analysis + ", description=" + description + "]";
	}
}
