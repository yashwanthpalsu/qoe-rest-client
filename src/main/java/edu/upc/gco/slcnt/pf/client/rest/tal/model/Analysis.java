package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Analysis {

	@JsonProperty("threshold")
	private List<Threshold> threshold;
	
	@JsonProperty("boolOp")
	private String boolOp;
	
	@JsonProperty("OID")
	private String OID;
	
	@JsonProperty("description")
	private String description;

	public List<Threshold> getThreshold() {
		return threshold;
	}

	public void setThreshold(List<Threshold> threshold) {
		this.threshold = threshold;
	}

	public String getBoolOp() {
		return boolOp;
	}

	public void setBoolOp(String boolOp) {
		this.boolOp = boolOp;
	}

	public String getOID() {
		return OID;
	}

	public void setOID(String oID) {
		OID = oID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "[threshold=" + threshold + ", boolOp=" + boolOp + ", OID=" + OID + ", description="
				+ description + "]";
	}
}
