package edu.upc.gco.slcnt.pf.client.rest.tal.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Threshold {

	@JsonProperty("level")
	private List<Level> levels;
	
	@JsonProperty("comparison")
	private String comparison;
	
	@JsonProperty("OID")
	private String OID;
	
	@JsonProperty("logical")
	private String logical;

	public List<Level> getLevels() {
		return levels;
	}

	public void setLevels(List<Level> levels) {
		this.levels = levels;
	}

	public String getComparison() {
		return comparison;
	}

	public void setComparison(String comparison) {
		this.comparison = comparison;
	}

	public String getOID() {
		return OID;
	}

	public void setOID(String oID) {
		OID = oID;
	}

	public String getLogical() {
		return logical;
	}

	public void setLogical(String logical) {
		this.logical = logical;
	}

	@Override
	public String toString() {
		return "[levels=" + levels + ", comparison=" + comparison + ", OID=" + OID + ", logical=" + logical
				+ "]";
	}
}
