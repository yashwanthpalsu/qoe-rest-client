# QoE REST+ Client library

Spring-based REST (and more) Client.


## Overview  

This library provides a client API to contact different components to the SliceNet Framework via REST, WebSockets, etc.

List of the SliceNet components that can be currently contacted through this API:

- SliceNet CP
	- CPSR
- Plug & Play Control
- QoE Optimizer
- Policy Framework (work in progress)
- Data Lake (work in progress)
	- InfluxDB
- Orchestration (work in progress)
	- Open Source MANO
	- OpenStack

## Build

To compile the library use the command:

$ mvn compile

To package the library into a jar file use the command:

$ mvn package

To install the library in the local repository use the command:

$ mvn install

